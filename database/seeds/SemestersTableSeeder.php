<?php

use App\Semester;
use Illuminate\Database\Seeder;

class SemestersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Semester::create(['year' => '2019', 'period' => 'I', 'start_date' => '2019-04-01', 'end_date' => '2019-07-26']);
        Semester::create(['year' => '2019', 'period' => 'II', 'start_date' => '2019-08-30', 'end_date' => '2019-12-13']);
        Semester::create(['year' => '2020', 'period' => 'I', 'start_date' => '2020-04-01', 'end_date' => '2020-07-26']);
        Semester::create(['year' => '2020', 'period' => 'II', 'start_date' => '2020-08-30', 'end_date' => '2020-12-13']);
        Semester::create(['year' => '2021', 'period' => 'I', 'start_date' => '2021-04-01', 'end_date' => '2021-07-26']);
        Semester::create(['year' => '2021', 'period' => 'II', 'start_date' => '2021-08-30', 'end_date' => '2021-12-13']);
    }
}
