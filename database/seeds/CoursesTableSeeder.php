<?php

use App\Course;
use Illuminate\Database\Seeder;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Course::create(['name' => 'Técnicas de comunicación', 'cycle' => 'I', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4, 'module_id' =>1, 'career_id' => 3]);
        Course::create(['name' => 'Interpretación y producción de textos', 'cycle' => 'II', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4, 'module_id' =>1, 'career_id' => 3]);
        Course::create(['name' => 'Lógica y funciones', 'cycle' => 'I', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4, 'module_id' =>2, 'career_id' => 3]);
        Course::create(['name' => 'Estadística general', 'cycle' => 'II', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4, 'module_id' =>2, 'career_id' => 3]);
        Course::create(['name' => 'Estadística general', 'cycle' => 'III', 'credits' => 2, 'total_hours' => 54, 'evaluation_criteria' => 4, 'module_id' =>3, 'career_id' => 3]);
        Course::create(['name' => 'Medio ambiente y desarrollo sostenible', 'cycle' => 'III', 'credits' => 2, 'total_hours' => 54, 'evaluation_criteria' => 4, 'module_id' =>4, 'career_id' => 3]);
        Course::create(['name' => 'Cultura física y deporte', 'cycle' => 'I', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4, 'module_id' =>5, 'career_id' => 3]);
        Course::create(['name' => 'Cultura artística', 'cycle' => 'II', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4, 'module_id' =>5, 'career_id' => 3]);
        Course::create(['name' => 'Informática e internet', 'cycle' => 'I', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4, 'module_id' =>6, 'career_id' => 3]);
        Course::create(['name' => 'Ofimática', 'cycle' => 'II', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4,  'module_id' =>6, 'career_id' => 3]);
        Course::create(['name' => 'Comunicación interpersonal', 'cycle' => 'IV', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4, 'module_id' =>7, 'career_id' => 3]);
        Course::create(['name' => 'Comunicación empresarial', 'cycle' => 'V', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4, 'module_id' =>7, 'career_id' => 3]);
        Course::create(['name' => 'Fundamentos de investigación', 'cycle' => 'II', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4, 'module_id' =>8, 'career_id' => 3]);
        Course::create(['name' => 'Investigación e innovación tecnológica', 'cycle' => 'III', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4, 'module_id' =>8, 'career_id' => 3]);
        Course::create(['name' => 'Proyectos de investigación e innovación tecnológica', 'cycle' => 'IV', 'credits' => 3, 'total_hours' => 72, 'evaluation_criteria' => 4, 'module_id' =>8, 'career_id' => 3]);
        Course::create(['name' => 'Comportamiento ético', 'cycle' => 'V', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4, 'module_id' =>9, 'career_id' => 3]);
        Course::create(['name' => 'Liderazgo y trabajo en equipo', 'cycle' => 'VI', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4, 'module_id' =>9, 'career_id' => 3]);
        Course::create(['name' => 'Organización y constitución de empresas', 'cycle' => 'V', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4,  'module_id' =>10, 'career_id' => 3]);
        Course::create(['name' => 'Proyecto empresarial', 'cycle' => 'VI', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4,  'module_id' =>10, 'career_id' => 3]);
        Course::create(['name' => 'Legislación e insercción laboral', 'cycle' => 'VI', 'credits' => 2, 'total_hours' => 54, 'evaluation_criteria' => 4,  'module_id' =>11, 'career_id' => 3]);

        Course::create(['name' => 'Dibujo Técnico', 'cycle' => 'I', 'credits' => 3, 'total_hours' => 72, 'evaluation_criteria' => 4, 'module_id' => 12, 'career_id' => 3]);
        Course::create(['name' => 'Materiales industriales', 'cycle' => 'I', 'credits' => 2, 'total_hours' => 54, 'evaluation_criteria' => 4, 'module_id' => 12, 'career_id' => 3]);
        Course::create(['name' => 'Mecánica de Banco', 'cycle' => 'I', 'credits' => 5, 'total_hours' => 126, 'evaluation_criteria' => 4, 'module_id' => 12, 'career_id' => 3]);
        Course::create(['name' => 'Máquinas Básicas', 'cycle' => 'II', 'credits' => 3, 'total_hours' => 72, 'evaluation_criteria' => 4, 'module_id' => 12, 'career_id' => 3]);
        Course::create(['name' => 'Dibujo Asistido por Ordenador', 'cycle' => 'II', 'credits' => 3, 'total_hours' => 72, 'evaluation_criteria' => 4, 'module_id' => 12, 'career_id' => 3]);
        Course::create(['name' => 'Cálculo de Elementos de Máquinas', 'cycle' => 'II', 'credits' => 2, 'total_hours' => 54, 'evaluation_criteria' => 4, 'module_id' => 12, 'career_id' => 3]);

        Course::create(['name' => 'Dibujo y Cálculo de Soldaduras', 'cycle' => 'I', 'credits' => 2, 'total_hours' => 54, 'evaluation_criteria' => 4, 'module_id' => 13, 'career_id' => 3]);
        Course::create(['name' => 'Soldadura Oxigas', 'cycle' => 'I', 'credits' => 4, 'total_hours' => 90, 'evaluation_criteria' => 4, 'module_id' => 13, 'career_id' => 3]);
        Course::create(['name' => 'Soldadura Eléctrica', 'cycle' => 'II', 'credits' => 4, 'total_hours' => 108, 'evaluation_criteria' => 4, 'module_id' => 13, 'career_id' => 3]);
        Course::create(['name' => 'Soldadura Mixta', 'cycle' => 'II', 'credits' => 2, 'total_hours' => 54, 'evaluation_criteria' => 4, 'module_id' => 13, 'career_id' => 3]);

        Course::create(['name' => 'Máquinas Convencionales I', 'cycle' => 'III', 'credits' => 9, 'total_hours' => 216, 'evaluation_criteria' => 4, 'module_id' => 14, 'career_id' => 3]);
        Course::create(['name' => 'Máquinas Convencionales II', 'cycle' => 'IV', 'credits' => 9, 'total_hours' => 216, 'evaluation_criteria' => 4, 'module_id' => 14, 'career_id' => 3]);
        Course::create(['name' => 'Máquinas Especiales', 'cycle' => 'III', 'credits' => 4, 'total_hours' => 90, 'evaluation_criteria' => 4, 'module_id' => 14, 'career_id' => 3]);
        Course::create(['name' => 'Máquinas de Control Numérico Computarizado', 'cycle' => 'IV', 'credits' => 5, 'total_hours' => 126, 'evaluation_criteria' => 4, 'module_id' => 14, 'career_id' => 3]);
        Course::create(['name' => 'Técnicas de Producción I', 'cycle' => 'III', 'credits' => 4, 'total_hours' => 90, 'evaluation_criteria' => 4, 'module_id' => 14, 'career_id' => 3]);
        Course::create(['name' => 'Técnicas de Producción II', 'cycle' => 'IV', 'credits' => 4, 'total_hours' => 90, 'evaluation_criteria' => 4, 'module_id' => 14, 'career_id' => 3]);

        Course::create(['name' => 'Modelería y Fundición', 'cycle' => 'V', 'credits' => 5, 'total_hours' => 126, 'evaluation_criteria' => 4, 'module_id' => 15, 'career_id' => 3]);
        Course::create(['name' => 'Moldes Permanentes', 'cycle' => 'V', 'credits' => 4, 'total_hours' => 90, 'evaluation_criteria' => 4, 'module_id' => 15, 'career_id' => 3]);
        Course::create(['name' => 'Matrices de Chapas', 'cycle' => 'V', 'credits' => 6, 'total_hours' => 144, 'evaluation_criteria' => 4, 'module_id' => 15, 'career_id' => 3]);
        Course::create(['name' => 'Tratamientos Térmicos', 'cycle' => 'V', 'credits' => 3, 'total_hours' => 72, 'evaluation_criteria' => 4, 'module_id' => 15, 'career_id' => 3]);

        Course::create(['name' => 'Gestión del Mantenimiento', 'cycle' => 'VI', 'credits' => 2, 'total_hours' => 36, 'evaluation_criteria' => 4, 'module_id' => 16, 'career_id' => 3]);
        Course::create(['name' => 'Seguridad e Higiene Industrial', 'cycle' => 'VI', 'credits' => 2, 'total_hours' => 36, 'evaluation_criteria' => 4, 'module_id' => 16, 'career_id' => 3]);
        Course::create(['name' => 'Mantenimiento Mecánico', 'cycle' => 'VI', 'credits' => 6, 'total_hours' => 144, 'evaluation_criteria' => 4, 'module_id' => 16, 'career_id' => 3]);
        Course::create(['name' => 'Automatización', 'cycle' => 'VI', 'credits' => 6, 'total_hours' => 144, 'evaluation_criteria' => 4, 'module_id' => 16, 'career_id' => 3]);
        Course::create(['name' => 'Mantenimiento Eléctrico', 'cycle' => 'VI', 'credits' => 2, 'total_hours' => 54, 'evaluation_criteria' => 4, 'module_id' => 16, 'career_id' => 3]);

        //Contabilidad
        Course::create(['name' => 'Técnicas de comunicación', 'cycle' => 'I', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4,  'module_id' =>17, 'career_id' => 2]);
        Course::create(['name' => 'Interpretación y producción de textos', 'cycle' => 'II', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4,  'module_id' =>17, 'career_id' => 2]);
        Course::create(['name' => 'Lógica y funciones', 'cycle' => 'I', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4,  'module_id' =>18, 'career_id' => 2]);
        Course::create(['name' => 'Estadítica general', 'cycle' => 'II', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4,  'module_id' =>18, 'career_id' => 2]);
        Course::create(['name' => 'Estadítica general', 'cycle' => 'III', 'credits' => 2, 'total_hours' => 54, 'evaluation_criteria' => 4,  'module_id' =>19, 'career_id' => 2]);
        Course::create(['name' => 'Medio ambiente y desarrollo sostenible', 'cycle' => 'III', 'credits' => 2, 'total_hours' => 54, 'evaluation_criteria' => 4,  'module_id' =>20, 'career_id' => 2]);
        Course::create(['name' => 'Cultura física y deporte', 'cycle' => 'I', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4,  'module_id' =>21, 'career_id' => 2]);
        Course::create(['name' => 'Cultura artística', 'cycle' => 'II', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4,  'module_id' =>21, 'career_id' => 2]);
        Course::create(['name' => 'Informática e internet', 'cycle' => 'I', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4,  'module_id' =>22, 'career_id' => 2]);
        Course::create(['name' => 'Ofimática', 'cycle' => 'II', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4,  'module_id' =>22, 'career_id' => 2]);
        Course::create(['name' => 'Comunicación interpersonal', 'cycle' => 'IV', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4,  'module_id' =>23, 'career_id' => 2]);
        Course::create(['name' => 'Comunicación empresarial', 'cycle' => 'V', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4,  'module_id' =>23, 'career_id' => 2]);
        Course::create(['name' => 'Fundamentos de investigación', 'cycle' => 'II', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4,  'module_id' =>24, 'career_id' => 2]);
        Course::create(['name' => 'Investigación e innovación tecnológica', 'cycle' => 'III', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4,  'module_id' =>24, 'career_id' => 2]);
        Course::create(['name' => 'Proyectos de investigación e innovación tecnológica', 'cycle' => 'IV', 'credits' => 3, 'total_hours' => 72, 'evaluation_criteria' => 4,  'module_id' =>24, 'career_id' => 2]);
        Course::create(['name' => 'Comportamiento ético', 'cycle' => 'V', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4,  'module_id' =>25, 'career_id' => 2]);
        Course::create(['name' => 'Liderazgo y trabajo en equipo', 'cycle' => 'VI', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4,  'module_id' =>25, 'career_id' => 2]);
        Course::create(['name' => 'Organización y constitución de empresas', 'cycle' => 'V', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4,  'module_id' =>26, 'career_id' => 2]);
        Course::create(['name' => 'Proyecto empresarial', 'cycle' => 'VI', 'credits' => 1.5, 'total_hours' => 36, 'evaluation_criteria' => 4,  'module_id' =>26, 'career_id' => 2]);
        Course::create(['name' => 'Legislación e insercción laboral', 'cycle' => 'VI', 'credits' => 2, 'total_hours' => 54, 'evaluation_criteria' => 4,  'module_id' =>27, 'career_id' => 2]);

        Course::create(['name' => 'Contabilidad General I', 'cycle' => 'I', 'credits' => 4, 'total_hours' => 108, 'evaluation_criteria' => 4, 'module_id' => 28, 'career_id' => 2]);
        Course::create(['name' => 'Contabilidad General II', 'cycle' => 'II', 'credits' => 5, 'total_hours' => 126, 'evaluation_criteria' => 4, 'module_id' => 28, 'career_id' => 2]);
        Course::create(['name' => 'Pan Contable', 'cycle' => 'I', 'credits' => 3, 'total_hours' => 72, 'evaluation_criteria' => 4, 'module_id' => 28, 'career_id' => 2]);
        Course::create(['name' => 'Documentación Comercial y Contable', 'cycle' => 'I', 'credits' => 4, 'total_hours' => 90, 'evaluation_criteria' => 4, 'module_id' => 28, 'career_id' => 2]);
        Course::create(['name' => 'Administración Empresarial', 'cycle' => 'I', 'credits' => 2, 'total_hours' => 54, 'evaluation_criteria' => 4, 'module_id' => 28, 'career_id' => 2]);
        Course::create(['name' => 'Legisilación Laboral', 'cycle' => 'II', 'credits' => 3, 'total_hours' => 72, 'evaluation_criteria' => 4, 'module_id' => 28, 'career_id' => 2]);
        Course::create(['name' => 'Legisilación Comercial', 'cycle' => 'I', 'credits' => 3, 'total_hours' => 72, 'evaluation_criteria' => 4, 'module_id' => 28, 'career_id' => 2]);
        Course::create(['name' => 'Legisilación Tributaria', 'cycle' => 'II', 'credits' => 3, 'total_hours' => 72, 'evaluation_criteria' => 4, 'module_id' => 28, 'career_id' => 2]);

        Course::create(['name' => 'Fundamentos de Costos', 'cycle' => 'II', 'credits' => 4, 'total_hours' => 90, 'evaluation_criteria' => 4, 'module_id' => 29, 'career_id' => 2]);
        Course::create(['name' => 'Contabilidad de Costos', 'cycle' => 'III', 'credits' => 4, 'total_hours' => 108, 'evaluation_criteria' => 4, 'module_id' => 29, 'career_id' => 2]);
        Course::create(['name' => 'Contabilidad de Sociedades', 'cycle' => 'IV', 'credits' => 6, 'total_hours' => 144, 'evaluation_criteria' => 4, 'module_id' => 29, 'career_id' => 2]);
        Course::create(['name' => 'Contabilidad aplicada', 'cycle' => 'IV', 'credits' => 6, 'total_hours' => 144, 'evaluation_criteria' => 4, 'module_id' => 29, 'career_id' => 2]);
        Course::create(['name' => 'Técnica Presupuestal', 'cycle' => 'III', 'credits' => 4, 'total_hours' => 108, 'evaluation_criteria' => 4, 'module_id' => 29, 'career_id' => 2]);
        Course::create(['name' => 'Contabilidad Gubernamental I', 'cycle' => 'III', 'credits' => 4, 'total_hours' => 90, 'evaluation_criteria' => 4, 'module_id' => 29, 'career_id' => 2]);
        Course::create(['name' => 'Contabilidad Gubernamental II', 'cycle' => 'IV', 'credits' => 6, 'total_hours' => 144, 'evaluation_criteria' => 4, 'module_id' => 29, 'career_id' => 2]);
        Course::create(['name' => 'Aplicativos Informáticos', 'cycle' => 'III', 'credits' => 4, 'total_hours' => 90, 'evaluation_criteria' => 4, 'module_id' => 29, 'career_id' => 2]);

        Course::create(['name' => 'Formulación de Estados Financieros', 'cycle' => 'V', 'credits' => 4, 'total_hours' => 108, 'evaluation_criteria' => 4, 'module_id' => 30, 'career_id' => 2]);
        Course::create(['name' => 'Análisis e Interpretación de Estados Financieros', 'cycle' => 'VI', 'credits' => 4, 'total_hours' => 90, 'evaluation_criteria' => 4, 'module_id' => 30, 'career_id' => 2]);
        Course::create(['name' => 'Fundamentos de Finanzas', 'cycle' => 'V', 'credits' => 3, 'total_hours' => 72, 'evaluation_criteria' => 4, 'module_id' => 30, 'career_id' => 2]);
        Course::create(['name' => 'Finanzas Públicas', 'cycle' => 'VI', 'credits' => 3, 'total_hours' => 72, 'evaluation_criteria' => 4, 'module_id' => 30, 'career_id' => 2]);
        Course::create(['name' => 'Formulación y Evaluación de Proyectos', 'cycle' => 'V', 'credits' => 4, 'total_hours' => 90, 'evaluation_criteria' => 4, 'module_id' => 30, 'career_id' => 2]);
        Course::create(['name' => 'Planeamiento de la Auditoría', 'cycle' => 'V', 'credits' => 3, 'total_hours' => 72, 'evaluation_criteria' => 4, 'module_id' => 30, 'career_id' => 2]);
        Course::create(['name' => 'Técnicas y Procedimientos de Auditoría', 'cycle' => 'VI', 'credits' => 3, 'total_hours' => 72, 'evaluation_criteria' => 4, 'module_id' => 30, 'career_id' => 2]);
        Course::create(['name' => 'Contabilidad de Entidades Financieras I', 'cycle' => 'V', 'credits' => 4, 'total_hours' => 90, 'evaluation_criteria' => 4, 'module_id' => 30, 'career_id' => 2]);
        Course::create(['name' => 'Contabilidad de Entidades Financieras II', 'cycle' => 'VI', 'credits' => 4, 'total_hours' => 90, 'evaluation_criteria' => 4, 'module_id' => 30, 'career_id' => 2]);
        Course::create(['name' => 'Cálculo Financiero', 'cycle' => 'I', 'credits' => 0, 'total_hours' => 0, 'evaluation_criteria' => 4, 'module_id' => 30, 'career_id' => 2]);
    }
}
