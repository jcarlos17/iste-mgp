<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'first_name' => 'Administrador',
            'last_name' => 'MGP',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123123'),
            'role' => 0
        ]);

        User::create([
            'first_name' => 'Secretario académico',
            'last_name' => 'MGP',
            'email' => 'secretario_a@gmail.com',
            'password' => bcrypt('123123'),
            'role' => 1
        ]);

        User::create([
            'first_name' => 'Secretaria',
            'last_name' => 'MGP',
            'email' => 'secretaria@gmail.com',
            'password' => bcrypt('123123'),
            'role' => 2
        ]);

        User::create([
            'first_name' => 'Docente',
            'last_name' => 'MGP',
            'email' => 'docente@gmail.com',
            'password' => bcrypt('123123'),
            'role' => 3
        ]);
    }
}
