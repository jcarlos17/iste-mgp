<?php

use App\GroupModule;
use Illuminate\Database\Seeder;

class GroupModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        GroupModule::create(['name' => 'Módulos Transversales']);
        GroupModule::create(['name' => 'Módulos Técnico Profesionales']);
    }
}
