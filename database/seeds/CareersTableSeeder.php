<?php

use App\Career;
use Illuminate\Database\Seeder;

class CareersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Career::create(['name' => 'Computación e Informática']);
        Career::create(['name' => 'Contabilidad']);
        Career::create(['name' => 'Mecánica de producción']);
    }
}
