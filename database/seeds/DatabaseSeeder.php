<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(SemestersTableSeeder::class);
        $this->call(CareersTableSeeder::class);
        $this->call(GroupModulesTableSeeder::class);
        $this->call(ModulesTableSeeder::class);
        $this->call(CoursesTableSeeder::class);
        $this->call(PostulantsTableSeeder::class);
        $this->call(EnrollmentsTableSeeder::class);
    }
}
