<?php

use App\Career;
use App\Enrollment;
use App\Semester;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Faker\Factory as Faker;

class EnrollmentsTableSeeder extends Seeder
{

    public function run()
    {
        for ($i=5;$i<=14;$i++) {
            $careerId = rand(1,3);
            DB::table('enrollments')->insert([
                'code' => $this->createCode($careerId, 2),
                'postulant_id' => $i,
                'career_id' => $careerId,
                'semester_id' => 2,
                'cycle' => 'IV',
                'payment' => 168.93,
            ]);
        }
    }

    public function createCode($careerId, $semesterId)
    {
        $semester = Semester::findOrFail($semesterId);
        $career = Career::findOrFail($careerId);

        $count = Enrollment::where('career_id', $careerId)->where('semester_id', $semesterId)->count()+1;
        $initialCareer = strtoupper(substr($career->name, 0, 3));
        $number = $semester->period == 'I' ? '1':'2';
        $period = $semester->year .'0'. $number;
        $order = str_pad($count, 3, "0", STR_PAD_LEFT);

        return $initialCareer.$period.'-'.$order;
    }
}
