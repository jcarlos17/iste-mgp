<?php

use App\Module;
use Illuminate\Database\Seeder;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Modulos transversales
        Module::create(['name' => 'Comunicación', 'group_module_id' => 1, 'career_id' => 3]);
        Module::create(['name' => 'Matematica', 'group_module_id' => 1, 'career_id' => 3]);
        Module::create(['name' => 'Sociedad y Economía', 'group_module_id' => 1, 'career_id' => 3]);
        Module::create(['name' => 'Medio Ambiente y Desarrollo Sostenido', 'group_module_id' => 1, 'career_id' => 3]);
        Module::create(['name' => 'Actividades', 'group_module_id' => 1, 'career_id' => 3]);
        Module::create(['name' => 'Informática', 'group_module_id' => 1, 'career_id' => 3]);
        Module::create(['name' => 'Idioma Extranjero', 'group_module_id' => 1, 'career_id' => 3]);
        Module::create(['name' => 'Investigación Tecnológica', 'group_module_id' => 1, 'career_id' => 3]);
        Module::create(['name' => 'Relaciones en el Entorno de Trabajo', 'group_module_id' => 1, 'career_id' => 3]);
        Module::create(['name' => 'Gstión Empresarial', 'group_module_id' => 1, 'career_id' => 3]);
        Module::create(['name' => 'Formación y Orientación', 'group_module_id' => 1, 'career_id' => 3]);

        Module::create(['name' => 'MP No. 1 Diseño Mecánico', 'group_module_id' => 2, 'career_id' => 3]);
        Module::create(['name' => 'MP No. 2 Tecnología de la Soldadura', 'group_module_id' => 2, 'career_id' => 3]);
        Module::create(['name' => 'MP No. 3 Mecanizado con Máquinas Herramientas', 'group_module_id' => 2, 'career_id' => 3]);
        Module::create(['name' => 'MP No. 4 Matricería y Fundición', 'group_module_id' => 2, 'career_id' => 3]);
        Module::create(['name' => 'MP No. 5 Mantenimiento Mecánico', 'group_module_id' => 2, 'career_id' => 3]);


        //Modulos transversales
        Module::create(['name' => 'Comunicación', 'group_module_id' => 1, 'career_id' => 2]);
        Module::create(['name' => 'Matematica', 'group_module_id' => 1, 'career_id' => 2]);
        Module::create(['name' => 'Sociedad y Economía', 'group_module_id' => 1, 'career_id' => 2]);
        Module::create(['name' => 'Medio Ambiente y Desarrollo Sostenido', 'group_module_id' => 1, 'career_id' => 2]);
        Module::create(['name' => 'Actividades', 'group_module_id' => 1, 'career_id' => 2]);
        Module::create(['name' => 'Informática', 'group_module_id' => 1, 'career_id' => 2]);
        Module::create(['name' => 'Idioma Extranjero', 'group_module_id' => 1, 'career_id' => 2]);
        Module::create(['name' => 'Investigación Tecnológica', 'group_module_id' => 1, 'career_id' => 2]);
        Module::create(['name' => 'Relaciones en el Entorno de Trabajo', 'group_module_id' => 1, 'career_id' => 2]);
        Module::create(['name' => 'Gstión Empresarial', 'group_module_id' => 1, 'career_id' => 2]);
        Module::create(['name' => 'Formación y Orientación', 'group_module_id' => 1, 'career_id' => 2]);

        Module::create(['name' => 'MP No. 1 Procesos Contables', 'group_module_id' => 2, 'career_id' => 2]);
        Module::create(['name' => 'MP No. 2 Contabilidad Pública y Privada', 'group_module_id' => 2, 'career_id' => 2]);
        Module::create(['name' => 'MP No. 3 Análisis Financiero', 'group_module_id' => 2, 'career_id' => 2]);
    }
}
