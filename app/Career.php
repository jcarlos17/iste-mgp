<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Career extends Model
{
    public function modules()
    {
        return $this->hasMany(Module::class);
    }

    public function courses()
    {
        return $this->hasMany(Course::class);
    }

    public function workload_courses()
    {
        return $this->belongsToMany(Course::class, 'workloads');
    }

    public function enrollments()
    {
        return $this->hasMany(Enrollment::class);
    }

    public function getShortNameAttribute()
    {
        return strtoupper(substr($this->name, 0, 3));
    }
}
