<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    public function group()
    {
        return $this->belongsTo(GroupModule::class , 'group_module_id');
    }

    public function career()
    {
        return $this->belongsTo(Career::class);
    }

}
