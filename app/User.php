<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'role',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected  $appends = ['name_complete', 'code'];

    //relationship
    public function career()
    {
        return $this->belongsTo(Career::class);
    }

    public function workload_careers()
    {
        return $this->belongsToMany(Career::class, 'workloads', 'teacher_id', 'career_id')->distinct('id');

    }

    public function assistances()
    {
        return $this->hasMany(Assistance::class);
    }

    public function enrollments()
    {
        return $this->hasMany(Enrollment::class, 'postulant_id');
    }

    public function notes()
    {
        return $this->hasMany(Note::class);
    }

    //accessors
    public function getGenderNameAttribute() // accessor gender_name
    {
        if ($this->gender == 'M')
            return 'Masculino';
        if ($this->gender == 'F')
            return 'Femenino';

        return ''; // ?
    }

    public function getImageUrlAttribute()
    {
        if($this->image)
            return '/images/users/'.$this->image;
        else
            return '/images/users/avatar-1.jpg';
    }

    public function getIsAdminAttribute()
    {
        return $this->role == 0 || $this->role == 1;
    }

    public function getIsSecretaryAttribute()
    {
        return $this->role == 2;
    }

    public function getIsTeacherAttribute()
    {
        return $this->role == 3;
    }

    public function getNameCompleteAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }

    public  function getCodeAttribute()
    {
        $firstEnrollment = $this->enrollments()->first();
        if ($firstEnrollment)
            return $firstEnrollment->code;

        return null;
    }
}
