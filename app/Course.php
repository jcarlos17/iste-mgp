<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [
        'name', 'cycle', 'credits', 'total_hours', 'evaluation_criteria', 'career_id', 'module_id'
    ];

    public function module()
    {
        return $this->belongsTo(Module::class);
    }

    public function career()
    {
        return $this->belongsTo(Career::class);
    }
}
