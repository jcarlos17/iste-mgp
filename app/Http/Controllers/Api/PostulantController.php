<?php

namespace App\Http\Controllers\Api;

use App\Course;
use App\Enrollment;
use App\Semester;
use App\User;
use App\Workload;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PostulantController extends Controller
{
    public function show(Request $request)
    {
        $queryText = $request->input('queryText');

        if ($queryText)
            return User::where('role', 4)->where(function ($query) use ($queryText) {
                $query->where('first_name', 'like', "%$queryText%")
                    ->orWhere('last_name', 'like', "%$queryText%");
            })->select('id', DB::raw('CONCAT(first_name," ",last_name) as text'))->get();
        else
            return User::where('role', 4)->orderBy('first_name')->take(20)
                ->select('id', DB::raw('CONCAT(first_name," ",last_name) as text'))->get();
    }

    public function enrollment($career_id, $courseId, Request $request)
    {
        $date = $request->date;
        $course = Course::find($courseId);
        $dateCarbon = Carbon::createFromFormat('d/m/Y', $date);

        $semester = Semester::where('start_date', '<', $dateCarbon)->where('end_date', '>', $dateCarbon)->first();

        if (!$semester)
            return '';

        // $enrollments
        return Enrollment::with('postulant')->where('career_id', $career_id)
            ->where('cycle', $course->cycle)->where('semester_id', $semester->id)->get();
    }
}
