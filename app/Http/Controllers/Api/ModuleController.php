<?php

namespace App\Http\Controllers\Api;

use App\Module;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ModuleController extends Controller
{
    public function index($careerId,$moduleId)
    {
        $modules = Module::where('career_id', $careerId)->where('group_module_id', $moduleId)->get();

        return $modules;
    }
}
