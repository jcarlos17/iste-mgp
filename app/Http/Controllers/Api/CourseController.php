<?php

namespace App\Http\Controllers\Api;

use App\Career;
use App\Course;
use App\Workload;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CourseController extends Controller
{
    public function index($id)
    {
        $career = Career::find($id);

        return $career->courses()->orderBy('name')->get();
    }

    public function show($id)
    {
        $course = Course::find($id);

        return $course;
    }

    public function workload($careerId,$id)
    {
        $courseIds = Workload::where('teacher_id', $id)->where('career_id', $careerId)->pluck('course_id');

        return Course::whereIn('id', $courseIds)->orderBy('name')->get();
    }
}
