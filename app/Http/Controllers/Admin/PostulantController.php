<?php

namespace App\Http\Controllers\Admin;

use App\Career;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class PostulantController extends Controller
{
    public function index()
    {
        $postulants = User::where('role', 4)->get();
        return view('admin.postulants.index', compact('postulants'));
    }

    public function create()
    {
        $careers = Career::orderBy('name', 'asc')->get();
        return view('admin.postulants.create', compact('careers'));
    }

    public function store(Request $request)
    {
        $rules = [
            'dni' => 'required',
            'gender' => 'required',
            'first_name' => 'required|regex:/^[A-Za-z áéíóúÁÉÍÓÚ]+$/|max:255',
            'last_name' => 'required|regex:/^[A-Za-z áéíóúÁÉÍÓÚ]+$/|max:255',
            'career_id' => 'required',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6'
        ];
        $messages = [
            'dni.required' => 'Es necesario ingresar el dni.',
            'gender.required' => 'Es necesario ingresar el género.',
            'first_name.required' => 'Es indispensable ingresar el nombre.',
            'first_name.regex' => 'Ingrese nombres válidos',
            'first_name.max' => 'El nombre ingresado es demasiado extenso.',
            'last_name.required' => 'Es indispensable ingresar los apellidos.',
            'last_name.regex' => 'Ingrese apellidos válidos',
            'last_name.max' => 'Los apellidos ingresados son demasiado extenso.',
            'career_id.required' => 'Es indispensable seleccionar la carrera.',
            'email.required' => 'Es indispensable ingresar el e-mail.',
            'email.email' => 'El e-mail ingresado no es válido.',
            'email.max' => 'El e-mail es demasiado extenso.',
            'email.unique' => 'El e-mail ya se encuentra en uso.',
            'password.required' => 'Es necesario ingresar una contraseña',
            'password.min' => 'La contraseña debe presentar al menos 6 caracteres.',
        ];

        $this->validate($request, $rules, $messages);

        $postulant = new User();
        $postulant->first_name = $request->first_name;
        $postulant->last_name = $request->last_name;
        $postulant->gender = $request->gender;
        $postulant->dni = $request->dni;
        $postulant->address = $request->address;
        $postulant->career_id = $request->career_id;
        $postulant->phone = $request->phone;
        $postulant->cellphone = $request->cellphone;
        if ($request->hasFile('image')) {
            $postulantImage = $request->file('image');
            $name = uniqid().'.'.$postulantImage->getClientOriginalExtension();
            $path = public_path().'/images/users';
            $postulantImage->move($path, $name);

            $postulant->image = $name;
        }
        $postulant->email = $request->email;
        $postulant->password = bcrypt($request->password);
        $postulant->role = 4;
        $postulant->save();

        return redirect('/admin/postulants')->with('notification', 'Se registró correctamente');
    }

    public function edit($id)
    {
        $postulant = User::find($id);
        $careers = Career::orderBy('name', 'asc')->get();

        return view('admin.postulants.edit', compact('postulant', 'careers'));
    }

    public function update($id, Request $request)
    {
        $rules = [
            'dni' => 'required',
            'gender' => 'required',
            'first_name' => 'required|regex:/^[A-Za-z áéíóúÁÉÍÓÚ]+$/|max:255',
            'last_name' => 'required|regex:/^[A-Za-z áéíóúÁÉÍÓÚ]+$/|max:255',
            'career_id' => 'required',
            'email' => "required|email|max:255|unique:users,email,$id",
            'password' => 'nullable|min:6'
        ];
        $messages = [
            'dni.required' => 'Es necesario ingresar el dni.',
            'gender.required' => 'Es necesario ingresar el género.',
            'first_name.required' => 'Es indispensable ingresar el nombre.',
            'first_name.regex' => 'Ingrese nombres válidos',
            'first_name.max' => 'El nombre ingresado es demasiado extenso.',
            'last_name.required' => 'Es indispensable ingresar los apellidos.',
            'last_name.regex' => 'Ingrese apellidos válidos',
            'last_name.max' => 'Los apellidos ingresados son demasiado extenso.',
            'career_id.required' => 'Es indispensable seleccionar la carrera.',
            'email.required' => 'Es indispensable ingresar el e-mail.',
            'email.email' => 'El e-mail ingresado no es válido.',
            'email.max' => 'El e-mail es demasiado extenso.',
            'email.unique' => 'El e-mail ya se encuentra en uso.',
            'password.min' => 'La contraseña debe presentar al menos 6 caracteres.',
        ];

        $this->validate($request, $rules, $messages);

        $postulant = User::find($id);
        $postulant->first_name = $request->first_name;
        $postulant->last_name = $request->last_name;
        $postulant->gender = $request->gender;
        $postulant->dni = $request->dni;
        $postulant->address = $request->address;
        $postulant->career_id = $request->career_id;
        $postulant->phone = $request->phone;
        $postulant->cellphone = $request->cellphone;
        if ($request->hasFile('image')) {
            $pathDelete = public_path('/images/users/' . $postulant->image);
            File::delete($pathDelete);

            $postulantImage = $request->file('image');
            $name = uniqid().'.'.$postulantImage->getClientOriginalExtension();
            $path = public_path().'/images/users';
            $postulantImage->move($path, $name);

            $postulant->image = $name;
        }
        $postulant->email = $request->email;
        if($request->password)
            $postulant->password = bcrypt($request->password);

        $postulant->save();

        return redirect('admin/postulants')->with('notification', 'Se guardó los cambios correctamente');
    }

    public function delete($id)
    {
        User::find($id)->delete();

        return back()->with('notification', 'Se eliminó correctamente.');
    }
}
