<?php

namespace App\Http\Controllers\Admin;

use App\Career;
use App\GroupModule;
use App\Module;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ModuleController extends Controller
{
    public function index()
    {
        $modules = Module::orderBy('group_module_id', 'asc')->get();

        return view('admin.modules.index', compact('modules'));
    }

    public function create()
    {
        $groups = GroupModule::all();
        $careers = Career::orderBy('name', 'asc')->get();

        return view('admin.modules.create', compact('groups', 'careers'));
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'group_module_id' => 'required',
            'career_id' => 'required'
        ];

        $messages = [
            'name.required' => 'Es necesario ingresar el nombre de la carrera.',
            'group_module_id.required' => 'Es necesario seleccionar un grupo de módulo.',
            'career_id.required' => 'Es necesario seleccionar la carrera.'
        ];

        $this->validate($request, $rules, $messages);

        $module = new Module();
        $module->name = $request->name;
        $module->description = $request->description;
        $module->group_module_id = $request->group_module_id;
        $module->career_id = $request->career_id;
        $module->save();

        return redirect('admin/modules')->with('notification', 'Se registró correctamente el módulo');
    }

    public function edit($id)
    {
        $module = Module::find($id);
        $groups = GroupModule::all();
        $careers = Career::orderBy('name', 'asc')->get();

        return view('admin.modules.edit', compact('module', 'groups', 'careers'));
    }

    public function update($id, Request $request)
    {
        $rules = [
            'name' => 'required',
            'group_module_id.required' => 'required',
            'career_id.required' => 'required'
        ];

        $messages = [
            'name.required' => 'Es necesario ingresar el nombre de la carrera.',
            'group_module_id' => 'Es necesario seleccionar un grupo de módulo.',
            'career_id' => 'Es necesario seleccionar la carrera.'
        ];

        $this->validate($request, $rules, $messages);

        $module = Module::find($id);
        $module->name = $request->name;
        $module->description = $request->description;
        $module->group_module_id = $request->group_module_id;
        $module->career_id = $request->career_id;
        $module->save();

        return redirect('admin/modules')->with('notification', 'Se guardó los cambios correctamente.');
    }

    public function delete($id)
    {
        Module::find($id)->delete();

        return back()->with('notification', 'Se eliminó correctamente el módulo.');
    }
}
