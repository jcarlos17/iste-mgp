<?php

namespace App\Http\Controllers\Admin;

use App\Career;
use App\Enrollment;
use App\Semester;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class EnrollmentController extends Controller
{
    public function index(Request $request)
    {
        $query = Enrollment::with('postulant');
        $searchEnrollment = $request->search_enrollment;

        if ($searchEnrollment) {// when search by date
            $query = $query->whereHas('postulant', function($query) use($searchEnrollment) {
                    $query->where('dni','LIKE', '%'.$searchEnrollment.'%')
                        ->orWhere('last_name','LIKE', '%'.$searchEnrollment.'%')
                        ->orderBy('id', 'desc');
                });
        }

        $enrollments = $query->get();

        return view('admin.enrollment.index', compact('enrollments', 'searchEnrollment'));
    }

    public function create()
    {
        $semesters = Semester::all();
        $careers = Career::all();

        if (old('postulant_id')) {
            $postulant = User::find(old('postulant_id'));
        }

        return view('admin.enrollment.create', compact('semesters','careers','postulant'));
    }

    public function store(Request $request)
    {
        $rules = [
            'postulant_id' => 'required',
            'career_id' => 'required',
            'semester_id' => 'required',
            'cycle' => 'required',
            'payment' => 'required|numeric|min:1'
        ];

        $messages = [
            'postulant_id.required' => 'Es necesario seleccionar un postulante.',
            'career_id.required' => 'Es necesario seleccionar la carrera.',
            'semester_id.required' => 'Es necesario seleccionar el semestre.',
            'cycle.required' => 'Es necesrio seleccionar el ciclo.',
            'payment.required' => 'Es necesario ingresar el pago.'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $validator->after(function($validator) use ($request) {
            $existsUser = Enrollment::where('postulant_id', $request->postulant_id)->where('semester_id', $request->semester_id)->exists();
            if ($existsUser)
                $validator->errors()->add('postulant_id', 'El postulante seleccionado ya tiene registrado una carrera.');
        });
        if ($validator->fails())
            return back()->withErrors($validator->messages())->withInput();

        //code
        $postulantId = $request->postulant_id;
        $careerId = $request->career_id;
        $semesterId = $request->semester_id;

        // new enrollment
        $enrollment = new Enrollment();
        $enrollment->code = $this->createCode($careerId, $semesterId);
        $enrollment->postulant_id = $postulantId;
        $enrollment->career_id = $careerId;
        $enrollment->semester_id = $semesterId;
        $enrollment->cycle = $request->cycle;
        $enrollment->payment = $request->payment;
        $enrollment->save();

        $notification = 'Se registró correctamente la matrícula.';
        return redirect('/admin/enrollments')->with(compact('notification'));
    }

    public function edit($id)
    {
        $enrollment = Enrollment::findOrFail($id);
        $semesters = Semester::all();
        $careers = Career::all();

        return view('admin.enrollment.edit', compact('enrollment', 'semesters', 'careers'));
    }

    public function update($id, Request $request)
    {
        $rules = [
            'postulant_id' => 'required',
            'career_id' => 'required',
            'semester_id' => 'required',
            'cycle' => 'required',
            'payment' => 'required|numeric|min:1'
        ];

        $messages = [
            'postulant_id.required' => 'Es necesario seleccionar un postulante.',
            'career_id.required' => 'Es necesario seleccionar la carrera.',
            'semester_id.required' => 'Es necesario seleccionar el semestre.',
            'cycle.required' => 'Es necesrio seleccionar el ciclo.',
            'payment.required' => 'Es necesario ingresar el pago.'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);


        $validator->after(function($validator) use ($request, $id) {
            $existsUser = Enrollment::where('id', '<>', $id)->where('postulant_id', $request->postulant_id)->where('semester_id', $request->semester_id)->exists();
            if ($existsUser)
                $validator->errors()->add('postulant_id', 'El postulante seleccionado ya tiene registrado una carrera.');
        });
        if ($validator->fails())
            return back()->withErrors($validator->messages())->withInput();

        // new enrollment
        $enrollment = Enrollment::findOrFail($id);
        $enrollment->postulant_id = $request->postulant_id;
        $enrollment->career_id = $request->career_id;
        $enrollment->semester_id = $request->semester_id;
        $enrollment->cycle = $request->cycle;
        $enrollment->payment = $request->payment;
        $enrollment->save();

        $notification = 'Se guardaron los cambios correctamente.';
        return redirect('/admin/enrollments')->with(compact('notification'));
    }

    public function delete($id)
    {
        $enrollment = Enrollment::findOrFail($id);
        $enrollment->delete();

        return back()->with('notification', 'Se desactivó correctamente.');
    }

    public function createCode($careerId, $semesterId)
    {
        $semester = Semester::findOrFail($semesterId);
        $career = Career::findOrFail($careerId);

        $count = Enrollment::where('career_id', $careerId)->where('semester_id', $semesterId)->count()+1;
        $initialCareer = strtoupper(substr($career->name, 0, 3));
        $number = $semester->period == 'I' ? '1':'2';
        $period = $semester->year .'0'. $number;
        $order = str_pad($count, 3, "0", STR_PAD_LEFT);

        return $initialCareer.$period.'-'.$order;
    }
}
