<?php

namespace App\Http\Controllers\Admin;

use App\Career;
use App\Semester;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GraphicController extends Controller
{
    public function index(Request $request)
    {
        $semesterId = $request->semester_id;
        $semestersAll = Semester::all();
        $semesters = Semester::orderBy('year', 'desc')->orderBy('period', 'desc')->take('6')->get();

        $careers = Career::all();
        foreach ($careers as $career) {
            $enrollments = $career->enrollments();
            if($semesterId)
                $enrollments = $enrollments->where('semester_id', $semesterId);

            $career->enrollments_count = $enrollments->count();
        }

        return view('admin.graphics.index', compact('semesters', 'careers', 'semestersAll', 'semesterId'));
    }
}
