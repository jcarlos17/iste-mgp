<?php

namespace App\Http\Controllers\Admin;

use App\Career;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CareerController extends Controller
{
    public function index()
    {
        $careers = Career::all();

        return view('admin.careers.index', compact('careers'));
    }

    public function create()
    {
        return view('admin.careers.create');
    }

    public function store(Request $request)
    {
        $rules = [
          'name' => 'required'
        ];

        $messages = [
            'name.required' => 'Es necesario ingresar el nombre de la carrera.'
        ];

        $this->validate($request, $rules, $messages);

        $career = new Career();
        $career->name = $request->name;
        $career->save();

        return redirect('admin/careers')->with('notification', 'Se registró correctamente la carrera');
    }

    public function edit($id)
    {
        $career = Career::find($id);

        return view('admin.careers.edit', compact('career'));
    }

    public function update($id, Request $request)
    {
        $rules = [
            'name' => 'required'
        ];

        $messages = [
            'name.required' => 'Es necesario ingresar el nombre de la carrera.'
        ];

        $this->validate($request, $rules, $messages);

        $career = Career::find($id);
        $career->name = $request->name;
        $career->save();

        return view('admin/careers')->with('notification', 'Se guardó correctamente los cambios.');
    }

    public function delete($id)
    {
        Career::find($id)->delete();

        return back()->with('notification', 'Se eliminó correctamente la carrera.');
    }

    public function modules($id)
    {
        $career = Career::find($id);

        return view('admin.careers.modules', compact('career'));
    }
}
