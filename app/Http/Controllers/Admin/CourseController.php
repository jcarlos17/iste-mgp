<?php

namespace App\Http\Controllers\Admin;

use App\Career;
use App\Course;
use App\GroupModule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CourseController extends Controller
{
    public function index()
    {
        $courses = Course::all();
        return view('admin.courses.index', compact('courses'));
    }

    public function create()
    {
        $careers = Career::all();
        $groupModules = GroupModule::all();
        return view('admin.courses.create', compact('careers', 'groupModules'));
    }

    public function store(Request $request)
    {
        $rules = [
            'career_id' => 'required',
            'module_id' => 'required',
            'name' => 'required|string|max:255',
            'cycle' => 'required',
            'credits' => 'required|numeric|min:0.1',
            'total_hours' => 'required|numeric|min:0.1',
            'evaluation_criteria' => 'required|numeric|min:1|max:4'
        ];

        $messages = [
            'career_id.required' => 'Es necesario seleccionar una carrera.',
            'module_id.required' => 'Es necesario seleccionar un módulo transversal.',
            'name.required' => 'Es necesario ingresar el nombre del curso.',
            'name.max' => 'El nombre ingresado es demasiado extenso.',
            'cycle.required' => 'Es necesario seleccionar el ciclo.',
            'credits.required' => 'Es necesario ingresar los créditos.',
            'credits.numeric' => 'El crédito ingresado debe ser formato numérico.',
            'credits.min' => 'El crédito ingresado debe ser mayor que 0.',
            'total_hours.required' => 'Es necesario ingresar las horas totales.',
            'total_hours.numeric' => 'La Hora Total ingresada debe ser formato numérico.',
            'total_hours.min' => 'La Hora Total ingresada debe ser mayor que 0.',
            'evaluation_criteria.required' => 'Es necesario ingresar el criterio de evaluación.',
            'evaluation_criteria.numeric' => 'El criterio de evaluación ingresado debe ser formato numérico.',
            'evaluation_criteria.min' => 'El criterio de evaluación ingresado debe ser mayor que 0.',
            'evaluation_criteria.max' => 'El criterio de evaluación ingresado debe ser menor igual que 4.',
        ];

        $this->validate($request, $rules, $messages);

        $course = new Course();
        $data = $request->except('_token');
        $course->fill($data)->save();

        return redirect('admin/courses')->with('notificatión', 'Se registró correctamente.');
    }

    public function edit($id)
    {
        $course = Course::find($id);
        $careers = Career::all();
        $groupModules = GroupModule::all();

        $module = $course->module;
        $group = $module->group;

        return view('admin.courses.edit', compact('course', 'careers', 'groupModules', 'group'));
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'career_id' => 'required',
            'module_id' => 'required',
            'name' => 'required|string|max:255',
            'cycle' => 'required',
            'credits' => 'required|numeric|min:0.1',
            'total_hours' => 'required|numeric|min:0.1',
            'evaluation_criteria' => 'required|numeric|min:1|max:4'
        ];

        $messages = [
            'career_id.required' => 'Es necesario seleccionar una carrera.',
            'module_id.required' => 'Es necesario seleccionar un módulo transversal.',
            'name.required' => 'Es necesario ingresar el nombre del curso.',
            'name.max' => 'El nombre ingresado es demasiado extenso.',
            'cycle.required' => 'Es necesario seleccionar el ciclo.',
            'credits.required' => 'Es necesario ingresar los créditos.',
            'credits.numeric' => 'El crédito ingresado debe ser formato numérico.',
            'credits.min' => 'El crédito ingresado debe ser mayor que 0.',
            'total_hours.required' => 'Es necesario ingresar las horas totales.',
            'total_hours.numeric' => 'La Hora Total ingresada debe ser formato numérico.',
            'total_hours.min' => 'La Hora Total ingresada debe ser mayor que 0.',
            'evaluation_criteria.required' => 'Es necesario ingresar el criterio de evaluación.',
            'evaluation_criteria.numeric' => 'El criterio de evaluación ingresado debe ser formato numérico.',
            'evaluation_criteria.min' => 'El criterio de evaluación ingresado debe ser mayor que 0.',
            'evaluation_criteria.max' => 'El criterio de evaluación ingresado debe ser menor igual que 4.',
        ];

        $this->validate($request, $rules, $messages);

        $course = Course::find($id);
        $data = $request->except('_token');
        $course->fill($data)->save();

        return redirect('admin/courses')->with('notificatión', 'Se guardaron los cambios correctamente.');
    }

    public function delete($id)
    {
        Course::find($id)->delete();

        return back()->with('notification', 'Se eliminó correctamente.');
    }
}
