<?php

namespace App\Http\Controllers\Admin;

use App\Assistance;
use App\AssistanceDetail;
use App\Career;
use App\Enrollment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AssistanceController extends Controller
{
    public function index()
    {
        $user = auth()->user();

        $assistances = $user->assistances;

        return view('admin.assistances.index', compact('assistances'));
    }

    public function create()
    {
        $user = auth()->user();
        $careers = $user->workload_careers;
        if (old('career_id'))
            $oldCareer = Career::findOrFail(old('career_id'));


        return view('admin.assistances.create', compact('user','careers', 'oldCareer'));
    }

    public function store(Request $request)
    {
        $origDate = $request->date;
        $date = str_replace('/', '-', $origDate );
        $careerId = $request->career_id;
        $courseId = $request->course_id;
        $dateCarbon = date("Y-m-d", strtotime($date));

        $existsAssitance = Assistance::where('course_id',$courseId)
            ->where('career_id',$careerId )->where('date', $dateCarbon)->first();

        if($existsAssitance)
            return redirect('admin/assistances')->with('error', 'La asistencia que intenta registrar ya existe. #'.$existsAssitance->id);

        $assistance = new Assistance();
        $assistance->career_id = $request->career_id;
        $assistance->course_id = $request->course_id;
        $assistance->date = $dateCarbon;
        $assistance->user_id = auth()->id();
        $assistance->save();

        $enrollmentIds = $request->enrollmentIds;

        foreach ($enrollmentIds as $enrollmentId) {
            $detail = new AssistanceDetail();
            $detail->assistance = $request->input('assistance'.$enrollmentId);
            $detail->enrollment_id = $enrollmentId;
            $detail->assistance_id = $assistance->id;
            $detail->save();
        }

        return back()->with('notification', 'Se registró correctamente');
    }

    public function edit($id)
    {
        $user = auth()->user();
        $careers = $user->workload_careers;
        $assistance = Assistance::findOrFail($id);
        if (old('career_id'))
            $oldCareer = Career::findOrFail(old('career_id'));

        return view('admin.assistances.edit', compact('user','careers', 'oldCareer', 'assistance'));
    }

    public function update($id, Request $request)
    {
        $assistance = Assistance::findOrFail($id);
        $assistance->career_id = $request->career_id;
        $assistance->course_id = $request->course_id;
        $date = $request->date;
        $assistance->date = Carbon::createFromFormat('d/m/Y', $date);
        $assistance->save();

        $enrollmentIds = $request->enrollmentIds;
        $assistance->details()->delete();
        foreach ($enrollmentIds as $enrollmentId) {
            $detail = new AssistanceDetail();
            $detail->assistance = $request->input('assistance'.$enrollmentId);
            $detail->enrollment_id = $enrollmentId;
            $detail->assistance_id = $assistance->id;
            $detail->save();
        }

        return redirect('admin/assistances')->with('notification', 'Se guardaron los cambios correctamente');
    }

    public function delete($id)
    {
        $assistance = Assistance::findOrFail($id);
        $assistance->details()->delete();
        $assistance->delete();

        return back()->with('notification', 'Se eliminó correctamente.');
    }
}
