<?php

namespace App\Http\Controllers\Admin;

use App\Career;
use App\Enrollment;
use App\Note;
use App\NoteDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NoteController extends Controller
{
    public function index()
    {
        $user = auth()->user();

        $notes = $user->notes;

        return view('admin.notes.index', compact('notes'));
    }

    public function create()
    {
        $user = auth()->user();

        $careers = $user->workload_careers;
        if (old('career_id'))
            $oldCareer = Career::findOrFail(old('career_id'));


        return view('admin.notes.create', compact('user','careers', 'oldCareer'));
    }

    public function store(Request $request)
    {
        $note = new Note();
        $note->career_id = $request->career_id;
        $note->course_id = $request->course_id;
        $date = $request->date;
        $note->date = Carbon::createFromFormat('d/m/Y', $date);
        $note->user_id = auth()->id();
        $note->save();

        $enrollmentIds = $request->enrollmentIds;
        $notes1 = $request->notes1;
        $notes2 = $request->notes2;
        $notes3 = $request->notes3;
        $notes4 = $request->notes4;

        $course = $note->course;
        $evaluation = $course->evaluation_criteria;

        for ($i=0;$i<count($enrollmentIds);$i++) {
            $sum = 0;
            $notesOne = $notes1[$i] ? $notes1[$i] : 0;
            $notesTwo = $notes2[$i] ? $notes2[$i] : 0;
            $notesThree = $notes3[$i] ? $notes3[$i] : 0;
            $notesFour = $notes4[$i] ? $notes4[$i] : 0;

            $detail = new NoteDetail();
            if ($evaluation >= 1) {
                $detail->note_1 = $notesOne;
                $sum += $notesOne;
            }
            if ($evaluation >= 2) {
                $detail->note_2 = $notesTwo;
                $sum += $notesTwo;
            }
            if ($evaluation >= 3) {
                $detail->note_3 = $notesThree;
                $sum += $notesThree;
            }
            if ($evaluation >= 4) {
                $detail->note_4 = $notesFour;
                $sum += $notesFour;
            }
            $average = ($sum)/$evaluation;
            $detail->average = number_format($average, 2);
            $detail->enrollment_id = $enrollmentIds[$i];
            $detail->note_id = $note->id;
            $detail->save();
        }

        return redirect('admin/notes')->with('notification', 'Se registró correctamente');
    }

    public function edit($id)
    {
        $user = auth()->user();
        $careers = $user->workload_careers;
        $note = Note::findOrFail($id);
        $enrollments = Enrollment::with('postulant')->where('career_id', $note->career_id)
            ->where('cycle', $note->course->cycle)->get();
        if (old('career_id'))
            $oldCareer = Career::findOrFail(old('career_id'));

        return view('admin.notes.edit', compact('user','careers', 'oldCareer', 'note', 'enrollments'));
    }

    public function update($id, Request $request)
    {
        $note = Note::findOrFail($id);
        $note->career_id = $request->career_id;
        $note->course_id = $request->course_id;
        $date = $request->date;
        $note->date = Carbon::createFromFormat('d/m/Y', $date);
        $note->save();

        $enrollmentIds = $request->enrollmentIds;
        $notes1 = $request->notes1;
        $notes2 = $request->notes2;
        $notes3 = $request->notes3;
        $notes4 = $request->notes4;

        $note->details()->delete();

        $course = $note->course;
        $evaluation = $course->evaluation_criteria;

        for ($i=0;$i<count($enrollmentIds);$i++) {
            $sum = 0;
            $notesOne = isset($notes1[$i]) ? $notes1[$i] : 0;
            $notesTwo = isset($notes2[$i]) ? $notes2[$i] : 0;
            $notesThree = isset($notes3[$i]) ? $notes3[$i] : 0;
            $notesFour = isset($notes4[$i]) ? $notes4[$i] : 0;

            $detail = new NoteDetail();
            if ($evaluation >= 1) {
                $detail->note_1 = $notesOne;
                $sum += $notesOne;
            }
            if ($evaluation >= 2) {
                $detail->note_2 = $notesTwo;
                $sum += $notesTwo;
            }
            if ($evaluation >= 3) {
                $detail->note_3 = $notesThree;
                $sum += $notesThree;
            }
            if ($evaluation >= 4) {
                $detail->note_4 = $notesFour;
                $sum += $notesFour;
            }
            $average = ($sum)/$evaluation;
            $detail->average = number_format($average, 2);
            $detail->enrollment_id = $enrollmentIds[$i];
            $detail->note_id = $note->id;
            $detail->save();
        }

        return redirect('admin/notes')->with('notification', 'Se guardaron los cambios correctamente');
    }

    public function delete($id)
    {
        $note = Note::findOrFail($id);
        $note->details()->delete();
        $note->delete();

        return back()->with('notification', 'Se eliminó correctamente.');
    }
}
