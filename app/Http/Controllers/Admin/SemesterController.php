<?php

namespace App\Http\Controllers\Admin;

use App\Semester;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SemesterController extends Controller
{
    public function index()
    {
        $semesters = Semester::orderBy('year', 'asc')->get();

        return view('admin.semesters.index', compact('semesters'));
    }

    public function create()
    {
        return view('admin.semesters.create');
    }

    public function store(Request $request)
    {

        $rules = [
            'year' => 'required|min:1',
            'period' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
        ];
        $messages = [
            'year.required' => 'Es necesario ingresar el año.',
            'year.min' => 'Es año ingresado no tiene el formato correcto',
            'period.required' => 'Es necesario ingresar el periodo.',
            'start_date.required' => 'Es necesario ingresar la fecha de inicio.',
            'end_date.required' => 'Es necesario ingresar la fecha de fin.',
        ];

        $this->validate($request, $rules, $messages);

        $startDate = Carbon::createFromFormat('d/m/Y', $request->start_date);
        $endDate = Carbon::createFromFormat('d/m/Y', $request->end_date);

        $semester = new Semester();
        $semester->year = $request->year;
        $semester->period = $request->period;
        $semester->start_date = $startDate;
        $semester->end_date = $endDate;
        $semester->save();

        return redirect('/admin/semesters')->with('notification', 'Se registró correctamente');
    }

    public function edit($id)
    {
        $semester = Semester::find($id);

        return view('admin.semesters.edit', compact('semester'));
    }

    public function update($id, Request $request)
    {

        $rules = [
            'year' => 'required|min:1',
            'period' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
        ];
        $messages = [
            'year.required' => 'Es necesario ingresar el año.',
            'year.min' => 'Es año ingresado no tiene el formato correcto',
            'period.required' => 'Es necesario ingresar el periodo.',
            'start_date.required' => 'Es necesario ingresar la fecha de inicio.',
            'end_date.required' => 'Es necesario ingresar la fecha de fin.',
        ];

        $this->validate($request, $rules, $messages);

        $startDate = Carbon::createFromFormat('d/m/Y', $request->start_date);
        $endDate = Carbon::createFromFormat('d/m/Y', $request->end_date);

        $semester = Semester::find($id);
        $semester->year = $request->year;
        $semester->period = $request->period;
        $semester->start_date = $startDate;
        $semester->end_date = $endDate;
        $semester->save();

        return redirect('/admin/semesters')->with('notification', 'Se guardó los cambios correctamente');
    }

    public function delete($id)
    {
        Semester::find($id)->delete();

        return back()->with('notification', 'Se eliminó correctamente');
    }
}
