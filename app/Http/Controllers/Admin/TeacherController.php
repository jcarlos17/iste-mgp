<?php

namespace App\Http\Controllers\Admin;

use App\Career;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class TeacherController extends Controller
{
    public function index()
    {
        $teachers = User::where('role', 3)->get();
        return view('admin.teachers.index', compact('teachers'));
    }

    public function create()
    {
        $careers = Career::orderBy('name', 'asc')->get();

        return view('admin.teachers.create', compact('careers'));
    }

    public function store(Request $request)
    {
        $rules = [
            'dni' => 'required',
            'gender' => 'required',
            'first_name' => 'required|regex:/^[A-Za-z áéíóúÁÉÍÓÚ]+$/|max:255',
            'last_name' => 'required|regex:/^[A-Za-z áéíóúÁÉÍÓÚ]+$/|max:255',
            'career_id' => 'required',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6'
        ];
        $messages = [
            'dni.required' => 'Es necesario ingresar el dni.',
            'gender.required' => 'Es necesario ingresar el género.',
            'first_name.required' => 'Es indispensable ingresar el nombre.',
            'first_name.regex' => 'Ingrese nombres válidos',
            'first_name.max' => 'El nombre ingresado es demasiado extenso.',
            'last_name.required' => 'Es indispensable ingresar los apellidos.',
            'last_name.regex' => 'Ingrese apellidos válidos',
            'last_name.max' => 'Los apellidos ingresados son demasiado extenso.',
            'career_id.required' => 'Es indispensable seleccionar la carrera.',
            'email.required' => 'Es indispensable ingresar el e-mail.',
            'email.email' => 'El e-mail ingresado no es válido.',
            'email.max' => 'El e-mail es demasiado extenso.',
            'email.unique' => 'El e-mail ya se encuentra en uso.',
            'password.required' => 'Es necesario ingresar una contraseña',
            'password.min' => 'La contraseña debe presentar al menos 6 caracteres.',
        ];

        $this->validate($request, $rules, $messages);

        $teacher = new User();
        $teacher->first_name = $request->first_name;
        $teacher->last_name = $request->last_name;
        $teacher->gender = $request->gender;
        $teacher->dni = $request->dni;
        $teacher->address = $request->address;
        $teacher->phone = $request->phone;
        $teacher->cellphone = $request->cellphone;
        $teacher->career_id = $request->career_id;
        if ($request->hasFile('image')) {
            $teacherImage = $request->file('image');
            $name = uniqid().'.'.$teacherImage->getClientOriginalExtension();
            $path = public_path().'/images/users';
            $teacherImage->move($path, $name);

            $teacher->image = $name;
        }
        $teacher->email = $request->email;
        $teacher->password = bcrypt($request->password);
        $teacher->role = 3;
        $teacher->save();

        return redirect('/admin/teachers')->with('notification', 'Se registró correctamente');
    }

    public function edit($id)
    {
        $teacher = User::find($id);
        $careers = Career::orderBy('name', 'asc')->get();
        return view('admin.teachers.edit', compact('teacher', 'careers'));
    }

    public function update($id, Request $request)
    {
        $rules = [
            'dni' => 'required',
            'gender' => 'required',
            'first_name' => 'required|regex:/^[A-Za-z áéíóúÁÉÍÓÚ]+$/|max:255',
            'last_name' => 'required|regex:/^[A-Za-z áéíóúÁÉÍÓÚ]+$/|max:255',
            'career_id' => 'required',
            'email' => "required|email|max:255|unique:users,email,$id",
            'password' => 'nullable|min:6'
        ];
        $messages = [
            'dni.required' => 'Es necesario ingresar el dni.',
            'gender.required' => 'Es necesario ingresar el género.',
            'first_name.required' => 'Es indispensable ingresar el nombre.',
            'first_name.regex' => 'Ingrese nombres válidos',
            'first_name.max' => 'El nombre ingresado es demasiado extenso.',
            'last_name.required' => 'Es indispensable ingresar los apellidos.',
            'last_name.regex' => 'Ingrese apellidos válidos',
            'last_name.max' => 'Los apellidos ingresados son demasiado extenso.',
            'career_id.required' => 'Es indispensable seleccionar la carrera.',
            'email.required' => 'Es indispensable ingresar el e-mail.',
            'email.email' => 'El e-mail ingresado no es válido.',
            'email.max' => 'El e-mail es demasiado extenso.',
            'email.unique' => 'El e-mail ya se encuentra en uso.',
            'password.min' => 'La contraseña debe presentar al menos 6 caracteres.',
        ];

        $this->validate($request, $rules, $messages);

        $teacher = User::find($id);
        $teacher->first_name = $request->first_name;
        $teacher->last_name = $request->last_name;
        $teacher->gender = $request->gender;
        $teacher->dni = $request->dni;
        $teacher->address = $request->address;
        $teacher->phone = $request->phone;
        $teacher->cellphone = $request->cellphone;
        $teacher->career_id = $request->career_id;
        if ($request->hasFile('image')) {
            $pathDelete = public_path('/images/users/' . $teacher->image);
            File::delete($pathDelete);

            $teacherImage = $request->file('image');
            $name = uniqid().'.'.$teacherImage->getClientOriginalExtension();
            $path = public_path().'/images/users';
            $teacherImage->move($path, $name);

            $teacher->image = $name;
        }
        $teacher->email = $request->email;
        if($request->password)
            $teacher->password = bcrypt($request->password);

        $teacher->save();

        return redirect('admin/teachers')->with('notification', 'Se guardó los cambios correctamente');
    }

    public function delete($id)
    {
        User::find($id)->delete();

        return back()->with('notification', 'Se eliminó correctamente.');
    }

}
