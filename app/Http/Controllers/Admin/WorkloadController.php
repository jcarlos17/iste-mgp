<?php

namespace App\Http\Controllers\Admin;

use App\Career;
use App\Semester;
use App\User;
use App\Workload;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WorkloadController extends Controller
{
    public function index()
    {
        $workloads = Workload::all();
        return view('admin.workload.index', compact('workloads'));
    }

    public function create()
    {
        $teachers = User::where('role', 3)->get();
        $semesters = Semester::all();
        $careers = Career::all();

        $careerId = old('career_id');
        $oldCareer = [];
        if($careerId)
            $oldCareer = Career::find($careerId);

        return view('admin.workload.create', compact('teachers', 'semesters', 'careers', 'oldCareer'));
    }

    public function store(Request $request)
    {
        $rules = [
            'teacher_id' => 'required',
            'semester_id' => 'required',
            'career_id' => 'required',
            'course_id' => 'required',
        ];

        $messages = [
            'teacher_id.required' => 'Es necesario seleccionar un docente.',
            'semester_id.required' => 'Es necesario seleccionar un semestre.',
            'career_id.required' => 'Es necesario seleccionar una carrera.',
            'course_id.required' => 'Es necesario seleccionar un curso.',
        ];

        $this->validate($request, $rules, $messages);

        $workLoad = new Workload();
        $data = $request->except('_token');

        $workLoad->fill($data);
        $workLoad->save();

        return redirect('admin/workload')->with('notification', 'Se registró correctamente.');
    }

    public function edit($id)
    {
        $workload = Workload::find($id);
        $teachers = User::where('role', 3)->get();
        $semesters = Semester::all();
        $careers = Career::all();

        return view('admin.workload.edit', compact('workload','teachers', 'semesters', 'careers'));
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'teacher_id' => 'required',
            'semester_id' => 'required',
            'career_id' => 'required',
            'course_id' => 'required',
        ];

        $messages = [
            'teacher_id.required' => 'Es necesario seleccionar un docente.',
            'semester_id.required' => 'Es necesario seleccionar un semestre.',
            'career_id.required' => 'Es necesario seleccionar una carrera.',
            'course_id.required' => 'Es necesario seleccionar un curso.',
        ];

        $this->validate($request, $rules, $messages);

        $workLoad = Workload::find($id);
        $data = $request->except('_token');

        $workLoad->fill($data);
        $workLoad->save();

        return redirect('admin/workload')->with('notification', 'Se guardaron los cambios correctamente.');
    }

    public function delete($id)
    {
        Workload::find($id)->delete();

        return back()->with('notification', 'Se eliminó correctamente.');
    }
}
