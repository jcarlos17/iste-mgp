<?php

namespace App\Http\Controllers\Admin\Reports;

use App\Career;
use App\Enrollment;
use App\Semester;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Collection;

class EnrollmentController extends Controller
{
    public function index(Request $request)
    {
        $careerId = $request->career_id;
        $semesterId = $request->semester_id;
        $download = $request->download;

        $careers = Career::all();
        $semesters = Semester::all();
        $query = Enrollment::query();

        if ($careerId) {
            $query = $query->where('career_id', $careerId);
        }
        if ($semesterId) {
            $query = $query->where('semester_id', $semesterId);
        }

        if ($download){
            $data = $query->get();
            $table = (new Collection($data));
            $fileName = "reporte_matriculados.csv";
            $filePath = storage_path($fileName);
            $handle = fopen($filePath, 'w');
            fprintf($handle, chr(0xEF).chr(0xBB).chr(0xBF));
            fputcsv($handle, [
                'CÓDIGO', 'DNI', 'ALUMNO', 'CARRERA'
            ],";");
            foreach($table as $enrollment) {
                fputcsv($handle, [
                    $enrollment->postulant->code,
                    $enrollment->postulant->dni,
                    $enrollment->postulant->name_complete,
                    $enrollment->career->name,
                ],";");
            }
            fclose($handle);
            $headers = [
                'Content-Type' => 'text/csv',
            ];
            return Response::download($filePath, $fileName, $headers);
        }

        $enrollments = $query->paginate(10);

        return view('admin.reports.enrollments', compact('enrollments', 'careers', 'semesters', 'careerId', 'semesterId'));
    }
}
