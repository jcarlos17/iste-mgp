<?php

namespace App\Http\Controllers\Admin\Reports;

use App\Career;
use App\Course;
use App\Enrollment;
use App\Note;
use App\NoteDetail;
use App\Semester;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Collection;

class NoteController extends Controller
{
    public function index(Request $request)
    {
        $filter = $request->filter;
        $postulantId = $request->postulant_id;
        $semesterId = $request->semester_id;
        $courseId = $request->course_id;
        $download = $request->download;

        $postulantIds = Enrollment::pluck('postulant_id');
        $postulants = User::whereIn('id', $postulantIds)->get();
        $semesters = Semester::all();
        $courseIds = Note::pluck('course_id');
        $courses = Course::whereIn('id', $courseIds)->orderBy('name')->get();

        $query = NoteDetail::with('enrollment');

        if ($postulantId) {
            $query = $query->whereHas('enrollment', function($query) use ($postulantId) {
                $query->where('postulant_id', $postulantId);
            });
        }
        if ($semesterId) {
            $query = $query->whereHas('enrollment', function($query) use ($semesterId) {
                $query->where('semester_id', $semesterId);
            });
        }

        if($courseId) {
            $noteIds = Note::where('course_id', $courseId)->pluck('id');
            $query = $query->whereIn('note_id', $noteIds);
        }


        if ($download){
            $data = $query->get();
            $table = (new Collection($data));
            $fileName = "reporte_notas.csv";
            $filePath = storage_path($fileName);
            $handle = fopen($filePath, 'w');
            fprintf($handle, chr(0xEF).chr(0xBB).chr(0xBF));
            fputcsv($handle, [
                'CÓDIGO', 'ALUMNO', 'CARRERA', 'CURSO', 'CE1', 'CE2', 'CE3', 'CE4', 'PROMEDIO'
            ],";");
            foreach($table as $detail) {
                fputcsv($handle, [
                    $detail->enrollment->postulant->code,
                    $detail->enrollment->postulant->name_complete,
                    $detail->enrollment->career->name,
                    $detail->note->course->name,
                    $detail->note_1,
                    $detail->note_2,
                    $detail->note_3,
                    $detail->note_4,
                    $detail->average
                ],";");
            }
            fclose($handle);
            $headers = [
                'Content-Type' => 'text/csv',
            ];
            return Response::download($filePath, $fileName, $headers);
        }

        $notes = $query->paginate(10);

        return view('admin.reports.notes', compact('notes', 'postulants', 'semesters', 'courses', 'postulantId', 'semesterId', 'courseId', 'filter'));
    }
}
