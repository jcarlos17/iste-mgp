<?php

namespace App\Http\Controllers\Admin\Reports;

use App\Assistance;
use App\AssistanceDetail;
use App\Career;
use App\Course;
use App\Enrollment;
use App\Note;
use App\NoteDetail;
use App\Semester;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Collection;

class AssistanceController extends Controller
{
    public function index(Request $request)
    {
        $careerId = $request->career_id;
        $semesterId = $request->semester_id;
        $courseId = $request->course_id;
        $download = $request->download;

        $careers = Career::all();
        $semesters = Semester::all();
        $courseIds = Assistance::pluck('course_id');
        $courses = Course::whereIn('id', $courseIds)->orderBy('name')->get();
        $query = AssistanceDetail::with('enrollment');

        $assistanceDetails = [];
        $assistanceDates = [];

        if ($semesterId && $courseId && $semesterId) {
            $query = $query->whereHas('enrollment', function($query) use ($semesterId) {
                $query->where('semester_id', $semesterId);
            });

            $assistanceIds = $query->distinct('assistance_id')->pluck('assistance_id');

            $assistanceDates = DB::table('assistances')
                ->where('career_id', $careerId)
                ->where('course_id', $courseId)
                ->whereIn('id', $assistanceIds)
                ->select('id', DB::raw("DATE_FORMAT(date, '%d/%m') new_date"))
                ->orderBy('date')
                ->get();

            $enrollmentIds = $query->select('enrollment_id')->groupBy('enrollment_id')->pluck('enrollment_id');

            foreach ($enrollmentIds as $enrollmentId){
                $assistanceDetails[$enrollmentId] = AssistanceDetail::where('enrollment_id', $enrollmentId)->first();

                foreach ($assistanceDates as $assistanceDate) {
                    $a = DB::table('assistance_details')
                        ->where('enrollment_id', $enrollmentId)
                        ->where('assistance_id', $assistanceDate->id)
                        ->select('assistance')
                        ->first();
                    $assistanceDetails[$enrollmentId][$assistanceDate->id] = $a;
                    $assistanceText = $a->assistance ? 'A' : 'F';

                    $assistanceDetails[$enrollmentId]['text'] = $assistanceDetails[$enrollmentId]['text']."$assistanceText,";
                }
            }

            $header = "'CÓDIGO', 'DNI', 'ALUMNO', 'CARRERA','CURSO', 'ASISTENCIA',";
            foreach ($assistanceDates as $assistanceDate) {
                $header = $header."$assistanceDate->new_date,";
            }

            if ($download) {
                $data = $assistanceDetails;
                $table = (new Collection($data));
                $fileName = "reporte_asistencias.csv";
                $filePath = storage_path($fileName);
                $handle = fopen($filePath, 'w');
                fprintf($handle, chr(0xEF).chr(0xBB).chr(0xBF));
                fputcsv($handle, [$header],";");
                foreach($table as $detail) {
                    fputcsv($handle, [
                        $detail->enrollment->postulant->code,
                        $detail->enrollment->postulant->dni,
                        $detail->enrollment->postulant->name_complete,
                        $detail->enrollment->career->name,
                        $detail->assistant->course->name,
                        $detail->text
                    ],";");
                }
                fclose($handle);
                $headers = [
                    'Content-Type' => 'text/csv',
                ];
                return Response::download($filePath, $fileName, $headers);
            }
        }


        return view('admin.reports.assistances', compact('assistanceDates','assistanceDetails', 'careers', 'semesters', 'courses', 'careerId', 'semesterId', 'courseId'));
    }
}
