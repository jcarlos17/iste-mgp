<?php

namespace App\Http\Middleware;

use Closure;

class SecretaryMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->role != 2 and auth()->user()->role != 0 and auth()->user()->role != 1) //No es secretaria
            return redirect('home');
        return $next($request);
    }
}
