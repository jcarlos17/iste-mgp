<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Semester extends Model
{
    public function enrollments()
    {
        return $this->hasMany(Enrollment::class);
    }

    public function getStartFormatAttribute()
    {
        return date('d/m/Y', strtotime($this->start_date));
    }

    public function getEndFormatAttribute()
    {
        return date('d/m/Y', strtotime($this->end_date));
    }

    public function getYearPeriodAttribute()
    {
        return $this->year.' - '.$this->period;
    }
}
