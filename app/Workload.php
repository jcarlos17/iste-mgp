<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workload extends Model
{
    protected $fillable = [
        'teacher_id', 'semester_id', 'career_id', 'course_id'
    ];

    public function teacher()
    {
        return $this->belongsTo(User::class, 'teacher_id');
    }

    public function semester()
    {
        return $this->belongsTo(Semester::class, 'semester_id');
    }

    public function career()
    {
        return $this->belongsTo(Career::class, 'career_id');
    }

    public function course()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }
}
