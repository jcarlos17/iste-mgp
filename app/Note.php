<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    public function career()
    {
        return $this->belongsTo(Career::class);
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function details()
    {
        return $this->hasMany(NoteDetail::class, 'note_id');
    }


    public function getDateFormatAttribute()
    {
        return date('d/m/Y', strtotime($this->date));
    }
}
