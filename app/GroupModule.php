<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupModule extends Model
{
    public $timestamps = false;

    public function modules()
    {
        return $this->hasMany(Module::class);
    }
}
