<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Enrollment extends Model
{
    use SoftDeletes;

    public function postulant()
    {
        return $this->belongsTo(User::class, 'postulant_id');
    }

    public function semester()
    {
        return $this->belongsTo(Semester::class);
    }

    public function career()
    {
        return $this->belongsTo(Career::class);
    }
}
