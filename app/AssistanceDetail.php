<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssistanceDetail extends Model
{
    public function assistant()
    {
        return $this->belongsTo(Assistance::class, 'assistance_id');
    }

    public function enrollment()
    {
        return $this->belongsTo(Enrollment::class);
    }

    public function getDescriptionAttribute()
    {
//        setlocale(LC_TIME, 'spanish');
//        $date = strftime("%d %b", strtotime($this->assistant->date));
        if($this->assistance)
            return 'A';

        return 'F';
    }
}
