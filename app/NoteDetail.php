<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoteDetail extends Model
{
    public function enrollment()
    {
        return $this->belongsTo(Enrollment::class);
    }

    public function note()
    {
        return $this->belongsTo(Note::class);
    }
}
