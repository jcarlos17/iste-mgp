<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">

    <!-- App Favicon -->
    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}">

    <!-- form Uploads -->
    <link href="{{ asset('plugins/fileuploads/css/dropify.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- App title -->
    <title>{{ config('app.name') }}</title>

    <!-- Custom box css -->
    <link href="{{ asset('plugins/custombox/dist/custombox.min.css') }}" rel="stylesheet">

    @yield('styles')

    <!-- App css -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/core.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/components.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/icons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/pages.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/menu.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="{{ asset('/css/fondo_edit.css') }}">

    <script src="{{ asset('js/modernizr.min.js') }}"></script>

</head>

<body class="fixed-left">
<!-- Begin page -->
<div id="wrapper">
    <!-- Top Bar Start -->
    <div class="topbar">
        <!-- LOGO -->
        <div class="topbar-left">
            <a href="#" class="logo"><span>Iste<span>MGP</span></span><i class="zmdi zmdi-layers"></i></a>
        </div>
        <!-- Button mobile view to collapse sidebar menu -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">
                <!-- Page title -->
                <ul class="nav navbar-nav navbar-left">
                    <li>
                        <button class="button-menu-mobile open-left">
                            <i class="zmdi zmdi-menu"></i>
                        </button>
                    </li>
                    <li>
                        <h6 class="page-title">
                            @yield('page-title', 'Bienvenido')
                        </h6>
                    </li>
                </ul>
                @yield('search')
            </div><!-- end container -->
        </div><!-- end navbar -->
    </div>

    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">
            <!-- User -->
            <div class="user-box">
                <form action="{{ url('/profile/image') }}" id="avatarForm">
                    {{ csrf_field() }}
                    <input type="file" style="display: none" id="avatarInput">
                </form>
                <div class="wrap">
                    <div class="user-img">
                        <img src="{{ auth()->user()->image_url }}" alt="user-img" id="avatarImage" title="{{ auth()->user()->name }}" class="img-circle img-thumbnail img-responsive">
                    </div>
                    <div class="text_over_image" id="textToEdit">Editar</div>
                </div>
                <h5><a href="">{{ auth()->user()->name }}</a></h5>
                <h6>{{ auth()->user()->role_name }}</h6>
                <ul class="list-inline">
                    {{--<li>--}}
                        {{--<a href="#" title="Perfil">--}}
                            {{--<i class="zmdi zmdi-settings"></i>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    <li>
                        <a href="{{ url('/logout') }}"
                           onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();" title="Cerrar sesión">
                            <i class="zmdi zmdi-power"></i>
                        </a>
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </div>
            <!-- End User -->

            <!--- Sidemenu -->

        @include('includes.menu')
        <!-- Sidebar -->
            <div class="clearfix"></div>

        </div>
    </div>
    <div class="content-page">
        @yield('content')
        <footer class="footer">
            {{ date('Y') }} © {{ config('app.name') }}
        </footer>
    </div>

</div>

<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/detect.js') }}"></script>
<script src="{{ asset('js/fastclick.js') }}"></script>
<script src="{{ asset('js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('js/jquery.blockUI.js') }}"></script>
<script src="{{ asset('js/waves.js') }}"></script>
<script src="{{ asset('js/jquery.nicescroll.js') }}"></script>
<script src="{{ asset('js/jquery.scrollTo.min.js') }}"></script>

<!-- Plugins Js -->
<script src="{{ asset('plugins/bootstrap-inputmask/bootstrap-inputmask.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>

<!-- file uploads js -->
<script src="{{ asset('plugins/fileuploads/js/dropify.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-knob/jquery.knob.js') }}"></script>

<!-- Modal-Effect -->
<script src="{{ asset('plugins/custombox/dist/custombox.min.js') }}"></script>
<script src="{{ asset('plugins/custombox/dist/legacy.min.js') }}"></script>

<!-- App js -->
<script src="{{ asset('js/jquery.core.js') }}"></script>
<script src="{{ asset('js/jquery.app.js') }}"></script>

<!-- Upload image profile -->
{{--<script type="text/javascript" src="{{ asset('js/image-profile.js') }}"></script>--}}

<!-- Sweet alert 2 -->
<script src="https://unpkg.com/sweetalert2@7.3.0/dist/sweetalert2.all.js"></script>

<!-- moments -->
<script src="{{ asset('plugins/moment/min/moment-with-locales.min.js') }}"></script>
<script>
    $('time').each(function(i, e) {
        let time = moment($(e).attr('datetime')).locale('es');
        $(e).html(time.format('ll'));
    });
</script>
@yield('scripts')
</body>
</html>