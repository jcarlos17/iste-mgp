@extends('layouts.app')

@section('page-title')
    <a href="{{ url('/admin/careers') }}">Carreras</a> >
    Nuevo
@endsection
@section('styles')
@endsection
@section('content')
    <div class="content">
        <div class="container">
            @include('includes.alerts')
            <form role="form"  action="" method="POST">
                    @csrf
                <div class="row">
                    <div class="col-sm-6">
                        <div class="card-box">
                            <h4 class="m-t-0 header-title m-t-0 m-b-20"><b>Datos de la carrera</b></h4>
                            <div class="form-group">
                                <label for="name" class="control-label">Nombre</label>
                                <input type="text" placeholder="Nombre de la carrera" class="form-control" id="name" name="name" value="{{ old('name') }}">
                            </div>
                            <div class="form-group" >
                                <button class="btn btn-primary btn-bordred m-t-20">
                                    Registrar
                                </button>
                                <a href="{{ url('admin/careers') }}" class="btn btn-default btn-bordred m-t-20">
                                    Cancelar
                                </a>
                            </div>
                        </div>
                    </div><!-- end col -->
                </div>
                <!-- end row -->
            </form>
        </div> <!-- container -->
    </div> <!-- content -->
@endsection
@section('scripts')
@endsection
