@extends('layouts.app')

@section('icon')
@endsection

@section('page-title')
    <a href="{{ url('/admin/careers') }}">Carreras</a> > Módulos
@endsection

@section('content')
    <div class="content">
        <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">Lista de módulos de {{ $career->name }}</h4>
                            <div class="table-responsive">
                                <table class="table m-0">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nombre</th>
                                        <th>Tipo</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($career->modules as $module)
                                        <tr>
                                            <th scope="row">{{ $module->id }}</th>
                                            <td>{{ $module->name }}</td>
                                            <td>{{ $module->group->name }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!-- end col -->

                </div>
{{--                {{ $evaluators->render() }}--}}
        </div>
    </div>
@endsection

@section('scripts')
@endsection