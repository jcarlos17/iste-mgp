@extends('layouts.app')

@section('icon')
@endsection

@section('page-title')
Carreras
@endsection

@section('content')
    <div class="content">
        <div class="container">
            @if (session('notification'))
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('notification') }}
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-sm-8">
                    <a href="{{ url('admin/careers/create') }}" class="btn btn-success btn-md waves-effect waves-light m-b-30"
                       data-overlaySpeed="200" data-overlayColor="#36404a">Nueva carrera</a>
                </div>
            </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">Lista de carreras</h4>
                            <div class="table-responsive">
                                <table class="table m-0">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nombre</th>
                                        <th>Acciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($careers as $career)
                                            <tr>
                                                <th scope="row">{{ $career->id }}</th>
                                                <td>{{ $career->name }}</td>
                                                <td>
                                                    <a href="{{ url('admin/careers/'.$career->id.'/modules') }}" class="btn btn-sm btn-purple" title="Ver módulos">
                                                        <i class="fa fa-list"></i>
                                                    </a>
                                                    <a href="{{ url('admin/careers/'.$career->id.'/edit') }}" class="btn btn-sm btn-primary" title="Editar">
                                                        <i class="fa fa-pencil-square-o"></i>
                                                    </a>
                                                    <button class="btn btn-sm btn-danger" title="Eliminar" data-delete="{{ url('admin/careers/'.$career->id.'/delete') }}">
                                                        <i class="fa fa-trash o"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!-- end col -->

                </div>
{{--                {{ $evaluators->render() }}--}}
        </div>
    </div>
@endsection

@section('scripts')
    <!-- Sweet alert 2 -->
    <script src="https://unpkg.com/sweetalert2@7.3.0/dist/sweetalert2.all.js"></script>
    <script>
        $(document).ready(function() {
            $('[data-delete]').on('click', onClickCareerDelete);
        });

        function onClickCareerDelete() {
            let urlDelete = $(this).data('delete');
            swal({
                title: '¿Seguro que desea eliminar esta carrera?',
                text: "",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',

                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sí, eliminar!'
            }).then((result) => {
                if (result.value) {
                location.href = urlDelete;
            }
        });
        }
    </script>
@endsection