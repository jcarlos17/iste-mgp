@extends('layouts.app')

@section('icon')
@endsection

@section('page-title')
Matrícula
@endsection

@section('content')
    <div class="content">
        <div class="container">
            @if (session('notification'))
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('notification') }}
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-sm-8">
                    <a href="{{ url('admin/enrollments/create') }}" class="btn btn-success btn-md waves-effect waves-light m-b-30">
                        <i class="zmdi zmdi-account-add"></i> Nuevo Matrícula
                    </a>
                </div><!-- end col -->
                <form action="">
                    <div class="col-sm-4">
                        <div class="input-group">
                            <input type="text" name="search_enrollment" class="form-control" placeholder="Buscar por dni o apellido..." value="{{ $searchEnrollment }}">
                            <span class="input-group-btn">
                                <button type="submit" class="btn waves-effect waves-light btn-purple">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </div><!-- end col -->
                </form>
            </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">Lista de matrículas</h4>
                            <div class="table-responsive">
                                <table class="table m-0">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>DNI</th>
                                        <th>Apellidos</th>
                                        <th>Nombres</th>
                                        <th>Teléfono</th>
                                        <th>Carrera</th>
                                        <th>Semestre</th>
                                        <th>Pago</th>
                                        <th>Ciclo</th>
                                        <th>Acciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($enrollments as $enrollment)
                                        <tr>
                                            <th scope="row">{{ $enrollment->postulant->code }}</th>
                                            <td>{{ $enrollment->postulant->dni }}</td>
                                            <td>{{ $enrollment->postulant->last_name }}</td>
                                            <td>{{ $enrollment->postulant->first_name }}</td>
                                            <td>{{ $enrollment->postulant->phone }}</td>
                                            <td>{{ $enrollment->career->name }}</td>
                                            <td>{{ $enrollment->semester->year_period }}</td>
                                            <td>{{ $enrollment->payment }}</td>
                                            <td>{{ $enrollment->cycle }}</td>
                                            <td>
                                                <a href="{{ url('admin/enrollments/'.$enrollment->id.'/edit') }}" class="btn btn-sm btn-primary" title="Editar">
                                                    <i class="fa fa-pencil-square-o"></i>
                                                </a>
                                                <button class="btn btn-sm btn-danger" title="Eliminar" data-delete="{{ url('admin/enrollments/'.$enrollment->id.'/delete') }}">
                                                    <i class="fa fa-trash o"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!-- end col -->

                </div>
{{--                {{ $evaluators->render() }}--}}
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('[data-delete]').on('click', onClickEvaluatorDelete);
        });

        function onClickEvaluatorDelete() {
            let urlDelete = $(this).data('delete');
            swal({
                title: '¿Seguro que desea desactivar esta matrícula?',
                text: "",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',

                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sí, desactivar!'
            }).then((result) => {
                if (result.value) {
                    location.href = urlDelete;
                }
            });
        }
    </script>

    {{--<script>--}}
        {{--$(document).ready(function() {--}}
            {{--$('[data-restore]').on('click', onClickEvaluatorRestore);--}}
        {{--});--}}

        {{--function onClickEvaluatorRestore() {--}}
            {{--let urlRestore = $(this).data('restore');--}}
            {{--swal({--}}
                {{--title: '¿Seguro que desea restaurar este evaluador?',--}}
                {{--text: "Se restaurará con sus respectivos procesos",--}}
                {{--type: 'info',--}}
                {{--showCancelButton: true,--}}
                {{--confirmButtonColor: '#10c469',--}}
                {{--cancelButtonText: 'Cancelar',--}}
                {{--confirmButtonText: 'Si, restaurar!'--}}
            {{--}).then((result) => {--}}
                {{--if (result.value) {--}}
                    {{--location.href = urlRestore;--}}
                {{--}--}}
            {{--});--}}
        {{--}--}}
    {{--</script>--}}
@endsection