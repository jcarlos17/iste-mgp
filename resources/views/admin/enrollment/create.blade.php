@extends('layouts.app')

@section('page-title')
    <a href="{{ url('/admin/enrollment') }}">Matrículas</a> >
    Nuevo
@endsection
@section('styles')
    <link href="{{ asset('plugins/select2/dist/css/select2.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('plugins/select2/dist/css/select2-bootstrap.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="content">
        <div class="container">
            @if (session('notification'))
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('notification') }}
                </div>
            @endif

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form role="form"  action="" method="POST">
                @csrf
                <div class="row">
                    <div class="col-sm-6">
                        <div class="card-box">
                            <h4 class="m-t-0 header-title m-t-0 m-b-20"><b>Datos de la Matrícula</b></h4>
                            <div class="form-group">
                                <label for="postulant_id">Postulante</label>
                                <input type="hidden" name="postulant_id" class="select2 form-control" id="postulant_id">
                            </div>
                            <div class="form-group">
                                <label for="career_id" class="control-label">Carrera</label>
                                <select class="form-control" id="career_id" name="career_id">
                                    <option value="">Seleccione carrera</option>
                                    @foreach($careers as $career)
                                        <option value="{{ $career->id }}" {{ old('career_id') == $career->id ? 'selected' : '' }}>{{ $career->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="semester_id" class="control-label">Semestre</label>
                                        <select class="form-control" id="semester_id" name="semester_id">
                                            <option value="">Seleccione semestre</option>
                                            @foreach($semesters as $semester)
                                                <option value="{{ $semester->id }}" {{ old('semester_id') == $semester->id ? 'selected' : '' }}>{{ $semester->year_period }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="cycle" class="control-label">Ciclo</label>
                                        <select class="form-control" id="cycle" name="cycle">
                                            <option value="">Seleccione ciclo</option>
                                            <option value="I" {{ old('cycle') == 'I' ? 'selected' : '' }}>I</option>
                                            <option value="II" {{ old('cycle') == 'II' ? 'selected' : '' }}>II</option>
                                            <option value="III" {{ old('cycle') == 'III' ? 'selected' : '' }}>III</option>
                                            <option value="IV" {{ old('cycle') == 'IV' ? 'selected' : '' }}>IV</option>
                                            <option value="V" {{ old('cycle') == 'V' ? 'selected' : '' }}>V</option>
                                            <option value="VI" {{ old('cycle') == 'VI' ? 'selected' : '' }}>VI</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="payment" class="control-label">Pago</label>
                                        <input type="text" placeholder="" class="form-control" id="payment" name="payment" value="{{ old('payment') }}" onkeypress="return justNumbers(event);">
                                    </div>
                                </div>
                                {{--<div class="col-sm-6">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="cycle" class="control-label">Cursos</label>--}}
                                        {{--<a href="#" class="form-control btn btn-default btn-bordred">--}}
                                            {{--<i class="fa fa-list-alt"></i>--}}
                                        {{--</a>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            </div>
                            <div class="form-group" >
                                <button class="btn btn-primary btn-bordred m-t-15">
                                    Registrar
                                    <i class="fa fa-save"></i>
                                </button>
                                <a href="{{ url('admin/enrollments') }}" class="btn btn-default btn-bordred m-l-5 m-t-15">
                                    Cancelar
                                </a>
                            </div>
                        </div><!-- end row -->
                    </div>
                </div><!-- end col -->
                <!-- end row -->
            </form>
        </div> <!-- container -->
    </div> <!-- content -->
@endsection
@section('scripts')
    <script>
        function justNumbers(e)
        {
            var keynum = window.event ? window.event.keyCode : e.which;
            if (keynum === 8 || keynum === 46)
                return true;
            return /\d/.test(String.fromCharCode(keynum));
        }
    </script>
    <script src="{{ asset('plugins/select2/dist/js/select2.min.js') }}" type="text/javascript"></script>
    <script>
        $('#postulant_id').select2({
            placeholder: 'Busque por nombre y apellidos...',
            ajax: {
                url: '/api/postulants',
                dataType: 'json',
                delay: 250,

                data: function (query) {
                    return {
                        queryText: query
                    };
                },

                results: function (data) {
                    return {
                        results: data
                    };
                },

                cache: true
            }
        });
        @if (old('postulant_id'))
        $('#postulant_id').select2('data', {id:{{$postulant->id}}, text:'{{$postulant->name_complete}}'});
        @endif
    </script>
@endsection
