@extends('layouts.app')

@section('icon')
@endsection

@section('page-title')
Matriculados
@endsection

@section('content')
    <div class="content">
        <div class="container">
            @if (session('notification'))
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('notification') }}
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <form class="form-inline" role="form" action="" method="GET">
                    <div class="form-group">
                        <select name="career_id" id="" class="form-control">
                            <option value="">Seleccionar carrera</option>
                            @foreach($careers as $career)
                                <option value="{{ $career->id }}" {{ $career->id == $careerId ? 'selected' : '' }}>{{ $career->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group m-l-10">
                        <select name="semester_id" id="" class="form-control">
                            <option value="">Seleccionar semestre</option>
                            @foreach($semesters as $semester)
                                <option value="{{ $semester->id }}" {{ $semester->id == $semesterId ? 'selected' : '' }}>{{ $semester->year_period }}</option>
                            @endforeach
                        </select>
                    </div>

                    <button type="submit" class="btn btn-success waves-effect waves-light m-l-10 btn-md">Buscar</button>
                    <div class="pull-right">
                        <button type="submit" name="download" value="1" class="btn btn-sm btn-purple m-b-2">Exportar</button>
                        <button type="button" class="btn btn-sm btn-primary" onclick="PrintElem()">Imprimir</button>
                    </div>
                </form>
                <div class="row" id="divPrint">
                    <div class="col-lg-12">
                        <div class="card-box m-t-10">
                            <h4 class="header-title m-t-0 m-b-30">Lista de matriculados</h4>
                            <div class="table-responsive">
                                <table class="table m-0">
                                    <thead>
                                    <tr>
                                        <th>CÓDIGO</th>
                                        <th>DNI</th>
                                        <th>ALUMNO</th>
                                        <th>CARRERA</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($enrollments as $enrollment)
                                        <tr>
                                            <th scope="row">{{ $enrollment->postulant->code }}</th>
                                            <td>{{ $enrollment->postulant->dni }}</td>
                                            <td>{{ $enrollment->postulant->name_complete }}</td>
                                            <td>{{ $enrollment->career->name }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!-- end col -->

                </div>
                {{ $enrollments->render() }}
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('[data-delete]').on('click', onClickEvaluatorDelete);
        });

        function onClickEvaluatorDelete() {
            let urlDelete = $(this).data('delete');
            swal({
                title: '¿Seguro que desea desactivar esta matrícula?',
                text: "",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',

                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sí, desactivar!'
            }).then((result) => {
                if (result.value) {
                    location.href = urlDelete;
                }
            });
        }
    </script>
    <script>
        function PrintElem() {
            Popup($('#divPrint').html());
        }

        function Popup(data) {
            var mywindow = window.open('');
            mywindow.document.write('<html><head><title></title>');
            mywindow.document.write('<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />');
            mywindow.document.write('</head><body >');
            mywindow.document.write(data);
            mywindow.document.write('</body></html>');

            mywindow.print();
            mywindow.close();
        }
    </script>
@endsection