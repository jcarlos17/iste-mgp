@extends('layouts.app')

@section('icon')
@endsection

@section('page-title')
Notas
@endsection

@section('content')
    <div class="content">
        <div class="container">
            @if (session('notification'))
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('notification') }}
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <form class="form-inline" role="form" action="" method="GET">
                    <div class="form-group" id="filterPostulantTwo">
                        <select name="semester_id" id="" class="form-control">
                            <option value="">Seleccionar semestre</option>
                            @foreach($semesters as $semester)
                                <option value="{{ $semester->id }}" {{ $semester->id == $semesterId ? 'selected' : '' }}>{{ $semester->year_period }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group m-l-10" id="filterCourse">
                        <select name="course_id" id="" class="form-control">
                            <option value="">Seleccionar curso</option>
                            @foreach($courses as $course)
                                <option value="{{ $course->id }}" {{ $course->id == $courseId ? 'selected' : '' }}>{{ $course->name }}({{$course->career->short_name}})</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group m-l-10" id="filterPostulantOne">
                        <select name="postulant_id" id="" class="form-control">
                            <option value="">Seleccionar alumno</option>
                            @foreach($postulants as $postulant)
                                <option value="{{ $postulant->id }}" {{ $postulant->id == $postulantId ? 'selected' : '' }}>{{ $postulant->name_complete }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-success waves-effect waves-light m-l-10 btn-md">Buscar</button>
                    <div class="pull-right">
                        <button type="submit" name="download" value="1" class="btn btn-sm btn-purple m-b-2">Exportar</button>
                        <button type="button" class="btn btn-sm btn-primary" onclick="PrintElem()">Imprimir</button>
                    </div>
                </form>
                <div class="row" id="divPrint">
                    <div class="col-lg-12">
                        <div class="card-box m-t-10">
                            <h4 class="header-title m-t-0 m-b-30">Lista de notas</h4>
                            <div class="table-responsive">
                                <table class="table m-0">
                                    <thead>
                                    <tr>
                                        <th>CÓDIGO</th>
                                        <th>ALUMNO</th>
                                        <th>CARRERA</th>
                                        <th>CURSO</th>
                                        <th class="text-center">CE1</th>
                                        <th class="text-center">CE2</th>
                                        <th class="text-center">CE3</th>
                                        <th class="text-center">CE4</th>
                                        <th class="text-center">PROMEDIO</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($notes as $detail)
                                        <tr>
                                            <th scope="row">{{ $detail->enrollment->code }}</th>
                                            <td>{{ $detail->enrollment->postulant->name_complete }}</td>
                                            <td>{{ $detail->enrollment->career->name }}</td>
                                            <td>{{ $detail->note->course->name }}</td>
                                            <td class="text-center">{{ $detail->note_1 }}</td>
                                            <td class="text-center">{{ $detail->note_2 }}</td>
                                            <td class="text-center">{{ $detail->note_3 }}</td>
                                            <td class="text-center">{{ $detail->note_4 }}</td>
                                            <th class="text-center">{{ $detail->average }}</th>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!-- end col -->

                </div>
                {{ $notes->render() }}
        </div>
    </div>
@endsection

@section('scripts')
    {{--<script>--}}
        {{--function showOptions(value) {--}}
            {{--const $filterPostulantOne = $('#filterPostulantOne');--}}
            {{--const $filterPostulantTwo = $('#filterPostulantTwo');--}}
            {{--const $filterCourse = $('#filterCourse');--}}
            {{--$filterPostulantOne.hide();--}}
            {{--$filterPostulantTwo.hide();--}}
            {{--$filterCourse.hide();--}}

            {{--if (parseInt(value) === 1) {--}}
                {{--$filterPostulantOne.show();--}}
                {{--$filterPostulantTwo.show();--}}
                {{--$filterCourse.hide();--}}
            {{--}--}}
            {{--if (parseInt(value) === 2) {--}}
                {{--$filterPostulantOne.hide();--}}
                {{--$filterPostulantTwo.hide();--}}
                {{--$filterCourse.show();--}}
            {{--}--}}
        {{--}--}}
    {{--</script>--}}
    <script>
        function PrintElem() {
            Popup($('#divPrint').html());
        }

        function Popup(data) {
            var mywindow = window.open('');
            mywindow.document.write('<html><head><title></title>');
            mywindow.document.write('<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />');
            mywindow.document.write('</head><body >');
            mywindow.document.write(data);
            mywindow.document.write('</body></html>');

            mywindow.print();
            mywindow.close();
        }
    </script>
@endsection