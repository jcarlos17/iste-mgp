@extends('layouts.app')

@section('icon')
@endsection

@section('page-title')
Cargas Lectivas
@endsection

@section('content')
    <div class="content">
        <div class="container">
            @if (session('notification'))
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('notification') }}
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-sm-8">
                    <a href="{{ url('admin/workload/create') }}" class="btn btn-success btn-md waves-effect waves-light m-b-30"
                       data-overlaySpeed="200" data-overlayColor="#36404a"><i class="zmdi zmdi-account-add"></i> Nuevo carga lectiva</a>
                </div><!-- end col -->
                {{--<form action="">--}}
                    {{--<div class="col-sm-4">--}}
                        {{--<div class="input-group">--}}
                            {{--<input type="text" name="search_evaluator" class="form-control" placeholder="Buscar evaluador..." value="{{ $searchEvaluator }}">--}}
                            {{--<span class="input-group-btn">--}}
                                {{--<button type="submit" class="btn waves-effect waves-light btn-purple">--}}
                                    {{--<i class="fa fa-search"></i>--}}
                                {{--</button>--}}
                            {{--</span>--}}
                        {{--</div>--}}
                    {{--</div><!-- end col -->--}}
                {{--</form>--}}
            </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">Lista de cargas lectivas</h4>
                            <div class="table-responsive">
                                <table class="table m-0">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Docente</th>
                                        <th>Semestre</th>
                                        <th>Carrera</th>
                                        <th>Curso</th>
                                        <th>Acciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($workloads as $workload)
                                            <tr>
                                                <th>{{ $workload->id }}</th>
                                                <td>{{ $workload->teacher->name_complete }}</td>
                                                <td>{{ $workload->semester->year_period }}</td>
                                                <td>{{ $workload->career->name }}</td>
                                                <td>{{ $workload->course->name }}</td>
                                                <td>
                                                    <a href="{{ url('admin/workload/'.$workload->id.'/edit') }}" class="btn btn-sm btn-primary" title="Editar">
                                                        <i class="fa fa-pencil-square-o"></i>
                                                    </a>
                                                    <button class="btn btn-sm btn-danger" data-delete="{{ url('admin/workload/'.$workload->id.'/delete') }}" title="Eliminar">
                                                        <i class="fa fa-trash o"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!-- end col -->

                </div>
{{--                {{ $evaluators->render() }}--}}
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('[data-delete]').on('click', onClickWorkloadDelete);
        });

        function onClickWorkloadDelete() {
            let urlDelete = $(this).data('delete');
            swal({
                title: '¿Seguro que desea eliminar esta carga lectiva?',
                text: "",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',

                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sí, desactivar!'
            }).then((result) => {
                if (result.value) {
                    location.href = urlDelete;
                }
            });
        }
    </script>

    <script>
        $(document).ready(function() {
            $('[data-restore]').on('click', onClickEvaluatorRestore);
        });

        function onClickEvaluatorRestore() {
            let urlRestore = $(this).data('restore');
            swal({
                title: '¿Seguro que desea restaurar este evaluador?',
                text: "Se restaurará con sus respectivos procesos",
                type: 'info',
                showCancelButton: true,
                confirmButtonColor: '#10c469',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Si, restaurar!'
            }).then((result) => {
                if (result.value) {
                    location.href = urlRestore;
                }
            });
        }
    </script>
@endsection