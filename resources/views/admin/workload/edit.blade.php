@extends('layouts.app')

@section('page-title')
    <a href="{{ url('/admin/workload') }}">Cargas Lectivas</a> >
    Editar
@endsection
@section('styles')
@endsection
@section('content')
    <div class="content">
        <div class="container">
            @if (session('notification'))
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('notification') }}
                </div>
            @endif

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form role="form"  action="" method="POST">
                @csrf
                <div class="row">
                    <div class="col-sm-6">
                        <div class="card-box">
                            <h4 class="m-t-0 header-title m-t-0 m-b-20"><b>Datos de la carga lectiva</b></h4>
                            <div class="form-group">
                                <label for="teacher_id" class="control-label">Docente</label>
                                <select class="form-control" name="teacher_id" id="teacher_id">
                                    <option value="">Seleccione docente</option>
                                    @foreach($teachers as $teacher)
                                        <option value="{{ $teacher->id }}" {{ $workload->teacher_id == $teacher->id ? 'selected' : '' }}>{{ $teacher->name_complete }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="semester_id" class="control-label">Semestre</label>
                                <select class="form-control" name="semester_id" id="semester_id">
                                    <option value="">Seleccione Semestre</option>
                                    @foreach($semesters as $semester)
                                        <option value="{{ $semester->id }}" {{ $workload->semester_id == $semester->id ? 'selected' : '' }}>{{ $semester->year_period }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="select_career" class="control-label">Carrera</label>
                                <select class="form-control" name="career_id" id="select_career">
                                    <option value="">Seleccione Carrera</option>
                                    @foreach($careers as $career)
                                        <option value="{{ $career->id }}" {{ $workload->career_id == $career->id ? 'selected' : '' }}>{{ $career->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="select_course" class="control-label">Curso</label>
                                <select class="form-control" id="select_course" name="course_id">
                                    <option value="">Seleccione Curso</option>
                                    @foreach($workload->career->courses as $course)
                                        <option value="{{ $course->id }}" {{ $workload->course_id == $course->id ? 'selected' : '' }}>{{ $course->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary btn-bordred m-l-15 m-t-30">
                                    Guardas cambios
                                </button>
                                <a href="{{ url('admin/workload') }}" class="btn btn-default btn-bordred m-l-15 m-t-30">
                                    Cancelar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div> <!-- container -->
    </div> <!-- content -->
@endsection
@section('scripts')
    <script>
        $(function() {
            $('#select_career').on('change', onSelectCourseChange);
        });

        function onSelectCourseChange() {
            var careerId = $(this).val();
            if (!careerId) {
                $('#select_course').html('<option value="">Seleccione curso</option>');
                $('#select_course').prop('disabled', true);
                return;
            }
            // AJAX
            $.get('/api/career/'+careerId+'/courses', function (data) {
                var html_select = '<option value="">Seleccione curso</option>';
                for (var i=0; i<data.length; ++i)
                    html_select += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
                $('#select_course').html(html_select);
                $('#select_course').prop('disabled', false);
            });
        }
    </script>
@endsection
