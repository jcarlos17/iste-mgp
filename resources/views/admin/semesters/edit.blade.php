@extends('layouts.app')

@section('page-title')
    <a href="{{ url('/admin/semesters') }}">Semestres</a> >
    Nuevo
@endsection
@section('styles')
    <link href="{{ asset('plugins/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">

    <link href="{{ asset('plugins/fileuploads/css/dropify.min.css') }}"/>
@endsection
@section('content')
    <!-- Start content -->
    <div class="content">
        <div class="container">
            @include('includes.alerts')

            <form role="form"  action="" method="POST">
                @csrf
                <div class="row">
                    <div class="col-sm-6">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="m-t-0 header-title m-t-0 m-b-20"><b>Datos del semestre</b></h4>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="year" class="control-label">Año</label>
                                                <input type="text" class="form-control" id="year" name="year" data-mask="9999" value="{{ $semester->year }}">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="period" class="control-label">Periodo</label>
                                                <select id="period" class="form-control" name="period">
                                                    <option value="">Seleccione periodo</option>
                                                    <option value="I" {{ $semester->period = 'I' ? 'selected' : '' }}>I</option>
                                                    <option value="II" {{ $semester->period = 'II' ? 'selected' : '' }}>II</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Fecha inicio y fin</label>
                                        <div class="input-daterange input-group" id="date-range">
                                            <input type="text" class="form-control" name="start_date" value="{{ $semester->start_format }}">
                                            <span class="input-group-addon bg-primary b-0 text-white">a</span>
                                            <input type="text" class="form-control" name="end_date" value="{{ $semester->end_format }}">
                                        </div>
                                    </div>
                                    <div class="form-group" >
                                        <button class="btn btn-success btn-bordred m-t-30">
                                            Guardar cambios
                                            <i class="fa fa-save"></i>
                                        </button>
                                        <a href="{{ url('admin/semesters') }}" class="btn btn-default btn-bordred m-t-30">
                                            Cancelar
                                        </a>
                                    </div>
                                </div><!-- end col -->
                            </div><!-- end col -->
                        </div><!-- end row -->
                    </div>
                </div><!-- end col -->
                <!-- end row -->
            </form>
        </div> <!-- container -->
    </div> <!-- content -->
@endsection
@section('scripts')
    <script src="{{ asset('plugins/fileuploads/js/dropify.min.js') }}"></script>
    <script>
        $('.dropify').dropify({
            messages: {
                'default': 'Drag and drop a file here or click',
                'replace': 'Drag and drop or click to replace',
                'remove': 'Remove',
                'error': 'Ooops, something wrong appended.'
            },
            error: {
                'fileSize': 'The file size is too big (1M max).'
            }
        });
    </script>
    <script src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script>
        jQuery('#date-range').datepicker({
            toggleActive: true,
            format: 'dd/mm/yyyy'
        });
    </script>
@endsection
