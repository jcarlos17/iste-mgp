@extends('layouts.app')

@section('icon')
@endsection

@section('page-title')
Semestre
@endsection

@section('content')
    <div class="content">
        <div class="container">
            @include('includes.alerts')
            <div class="row">
                <div class="col-sm-8">
                    <a href="{{ url('admin/semesters/create') }}" class="btn btn-success btn-md waves-effect waves-light m-b-30"
                       data-overlaySpeed="200" data-overlayColor="#36404a">Nuevo semestre</a>
                </div><!-- end col -->
            </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">Lista de semestres</h4>
                            <div class="table-responsive">
                                <table class="table m-0">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Año</th>
                                        <th>Periodo</th>
                                        <th>Inicio</th>
                                        <th>Fin</th>
                                        <th>Acciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($semesters as $semester)
                                            <tr>
                                                <th scope="row">{{ $semester->id }}</th>
                                                <td>{{ $semester->year }}</td>
                                                <td>{{ $semester->period }}</td>
                                                <td>{{ $semester->start_format }}</td>
                                                <td>{{ $semester->end_format }}</td>
                                                <td>
                                                    <a href="{{ url('admin/semesters/'.$semester->id.'/edit') }}" class="btn btn-sm btn-primary" title="Editar">
                                                        <i class="fa fa-pencil-square-o"></i>
                                                    </a>
                                                    <button class="btn btn-sm btn-danger" title="Eliminar" data-delete="{{ url('admin/semesters/'.$semester->id.'/delete') }}">
                                                        <i class="fa fa-trash o"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!-- end col -->

                </div>
{{--                {{ $evaluators->render() }}--}}
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('[data-delete]').on('click', onClickSemesterDelete);
        });

        function onClickSemesterDelete() {
            let urlDelete = $(this).data('delete');
            swal({
                title: '¿Seguro que desea eliminar este semestre?',
                text: "",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',

                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sí, eliminar!'
            }).then((result) => {
                if (result.value) {
                    location.href = urlDelete;
                }
            });
        }
    </script>
@endsection