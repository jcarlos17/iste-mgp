@extends('layouts.app')

@section('icon')
@endsection

@section('page-title')
    Notas
@endsection

@section('content')
    <div class="content">
        <div class="container">
            @if (session('notification'))
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('notification') }}
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-sm-8">
                    <a href="{{ url('admin/notes/create') }}" class="btn btn-success btn-md waves-effect waves-light m-b-30"
                       data-overlaySpeed="200" data-overlayColor="#36404a">Registrar nueva nota</a>
                </div><!-- end col -->
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-box">
                        <h4 class="header-title m-t-0 m-b-30">Lista de notas</h4>
                        <div class="table-responsive">
                            <table class="table m-0">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Carrera</th>
                                    <th>Curso</th>
                                    <th>Fecha</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($notes as $note)
                                    <tr>
                                        <th scope="row">{{ $note->id }}</th>
                                        <td>{{ $note->career->name }}</td>
                                        <td>{{ $note->course->name }}</td>
                                        <td>{{ $note->date_format }}</td>
                                        <td>
                                            <a href="{{ url('admin/notes/'.$note->id.'/edit') }}" class="btn btn-sm btn-primary" title="Editar">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </a>
                                            <button class="btn btn-sm btn-danger" title="Eliminar" data-delete="{{ url('admin/notes/'.$note->id.'/delete') }}">
                                                <i class="fa fa-trash o"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('[data-delete]').on('click', onClickAssistanceDelete);
        });
        function onClickAssistanceDelete() {
            let urlDelete = $(this).data('delete');
            swal({
                title: '¿Seguro que desea eliminar esta nota?',
                text: "",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',

                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sí, eliminar!'
            }).then((result) => {
                if (result.value) {
                location.href = urlDelete;
            }
        });
        }
    </script>
@endsection