@extends('layouts.app')

@section('icon')
@endsection
@section('styles')
    <link href="{{ asset('plugins/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection

@section('page-title')
    Notas
@endsection

@section('content')
    <div class="content">
        <div class="container">
            @if (session('notification'))
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('notification') }}
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="" method="POST">
                @csrf
                <div class="card-box">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="select_career" class="control-label">Carrera</label>
                                <select class="form-control" name="career_id" id="select_career" data-user-id="{{$user->id}}" required>
                                    <option value="">Seleccione Carrera</option>
                                    @foreach($careers as $career)
                                        <option value="{{ $career->id }}" {{ old('career_id') == $career->id ? 'selected' : '' }}>{{ $career->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div><!-- end col -->
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="select_course" class="control-label">Curso</label>
                                <select class="form-control" id="select_course" name="course_id" required>
                                    <option value="">Seleccione curso</option>
                                    @if(old('career_id'))
                                        @foreach($oldCareer->workload_courses as $course)
                                            <option value="{{ $course->id }}" {{ old('course_id') == $course->id ? 'selected' : '' }}>{{ $course->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div><!-- end col -->
                        <div class="col-sm-4 m-b-10">
                            <div class="form-group">
                                <label class="control-label">Fecha</label>
                                <input type="text" class="form-control" name="date" id="datepicker-autoclose" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">Lista de matrículas</h4>
                    <div class="table-responsive">
                        <table class="table m-0">
                            <thead>
                            <tr id="headNotes">
                                <th class="col-sm-2">Código</th>
                                <th>Alumno</th>
                                <th>Ciclo</th>
                                <th>CE 1</th>
                                <th>CE 2</th>
                                <th>CE 3</th>
                                <th>CE 4</th>
                                <th>Promedio</th>
                            </tr>
                            </thead>
                            <tbody id="contentNotes">
                            </tbody>
                        </table>
                    </div>
                    <div id="errorMessage"></div>
                    <div class="form-group" >
                        <button class="btn btn-primary btn-bordred" style="display: none;" id="btnRegister">
                            Registrar
                        </button>
                        <a href="{{ url('admin/notes') }}" class="btn btn-default btn-bordred">Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        jQuery('#datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true
        });
    </script>
    <script>
        $(function() {
            $('#select_career').on('change', onSelectCourseChange);
            $('#datepicker-autoclose, #select_course, #select_career').on('change', onShowPostulants);
        });
        var evaluation = [];
        function onSelectCourseChange() {
            var careerId = $(this).val();
            const userId = $(this).data('user-id');
            if (!careerId) {
                $('#select_course').html('<option value="">Seleccione curso</option>');
                $('#select_course').prop('disabled', true);
                return;
            }
            // AJAX
            $.get('/api/career/'+careerId+'/'+userId+'/courses', function (data) {
                var html_select = '<option value="">Seleccione curso</option>';
                for (var i=0; i<data.length; ++i)
                    html_select += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
                $('#select_course').html(html_select);
                $('#select_course').prop('disabled', false);
            });
        }

        function onShowPostulants() {
            var careerId = $('#select_career').val();
            var courseId = $('#select_course').val();
            var $date = $('#datepicker-autoclose').val();
            const $content = $('#contentNotes');
            const $btnRegister = $('#btnRegister');
            const $message = $('#errorMessage');

            if (careerId && courseId && $date) {

                $content.append('<i class="fa fa-2x fa-spinner fa-spin"></i>');
                // AJAX
                $.get('/api/course/'+courseId, function (course) {
                    const $head = $('#headNotes');
                    $head.empty();
                    $head.append('<th class="col-sm-2">Código</th>\n' +
                        '<th>Alumno</th>\n' +
                        '<th>Ciclo</th>\n');
                    for($i=1;$i<=course.evaluation_criteria; $i++)
                        $head.append('<th>CE '+$i+'</th>\n');

                    $head.append('<th>Promedio</th>');

                    evaluation = course.evaluation_criteria;

                });
                // AJAX
                $.get('/api/postulants/'+careerId+'/'+courseId+'/?date='+$date, function (enrollments) {
                    $content.empty();
                    if (enrollments.length > 0) {
                        for (var i=0; i<enrollments.length; ++i) {
                            var enrollment = enrollments[i];
                            var $tr = $('<tr>');
                            $content.append($tr);

                            $tr.append('<th id="code">'+enrollment.postulant.code+'</th>\n' +
                                '<td id="name">'+enrollment.postulant.name_complete+'</td>\n' +
                                '<td id="cycle">'+enrollment.cycle+'</td>\n');

                            for($i=1;$i<=evaluation; $i++) {
                                $tr.append('<td class="col-sm-1">\n' +
                                    '<input type="number" class="form-control" min="0" max="20" step="any" id="note'+$i+enrollment.id+'" name="notes'+$i+'[]" value="0" onchange="getAverage('+enrollment.id+')">\n' +
                                    '</td>\n');
                            }

                            $tr.append('<td class="col-sm-1">' +
                                '<input type="hidden" value="'+enrollment.id+'" name="enrollmentIds[]">' +
                                '<input type="text" class="form-control" id="average'+enrollment.id+'" value="0" disabled>\n' +
                                '</td>\n' +
                                '</tr>');
                        }
                        $message.empty();
                        $btnRegister.show();
                    } else {
                        $message.html('No se encontraron alumnos inscritos a este curso');
                        $btnRegister.hide();
                    }
                });
            }
        }
    </script>
    <script>
        function getAverage(enrollmentId) {
            const $note1 = $('#note1'+enrollmentId);
            const $note2 = $('#note2'+enrollmentId);
            const $note3 = $('#note3'+enrollmentId);
            const $note4 = $('#note4'+enrollmentId);
            var $average = $('#average'+enrollmentId);

            var sum = 0;
            if(evaluation >= 4)
                sum += $note4.val() ? parseFloat($note4.val()) : 0;
            if(evaluation >= 3)
                sum += $note3.val() ? parseFloat($note3.val()) : 0;
            if(evaluation >= 2)
                sum += $note2.val() ? parseFloat($note2.val()) : 0;
            if(evaluation >= 1)
                sum += $note1.val() ? parseFloat($note1.val()) : 0;

            var average = ( sum ) / evaluation;

            $average.val(average.toFixed(2));
        }
    </script>
@endsection