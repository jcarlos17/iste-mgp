@extends('layouts.app')

@section('page-title')
    <a href="{{ url('/admin/postulants') }}">Postulantes</a> >
    Nuevo
@endsection
@section('styles')
    <link href="{{ asset('plugins/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">

    <link href="{{ asset('plugins/fileuploads/css/dropify.min.css') }}"/>
@endsection
@section('content')
    <!-- Start content -->
    <div class="content">
        <div class="container">
            @include('includes.alerts')
            <form role="form"  action="" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="m-t-0 header-title m-t-0 m-b-20"><b>Datos del docente</b></h4>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="dni" class="control-label">DNI</label>
                                        <input type="text" placeholder="" data-mask="99999999" class="form-control" id="dni" name="dni" value="{{ $postulant->dni }}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <p class="control-label font-600">Género</p>
                                    <div class="radio radio-inline">
                                        <input type="radio" id="man" value="M" name="gender" {{ $postulant->gender == 'M' ? 'checked' : '' }}>
                                        <label for="man"> Masculino </label>
                                    </div>
                                    <div class="radio radio-purple radio-inline">
                                        <input type="radio" id="woman" value="F" name="gender" {{ $postulant->gender == 'F' ? 'checked' : '' }}>
                                        <label for="woman"> Femenino </label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="first_name" class="control-label">Nombre(s)</label>
                                        <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Ingresar nombre" value="{{$postulant->first_name }}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="last_name" class="control-label">Apellidos</label>
                                        <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Ingresar apellidos" value="{{ $postulant->last_name }}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="address" class="control-label">Dirección</label>
                                <input type="text" class="form-control" id="address" name="address" placeholder="Ingresar dirección" value="{{ $postulant->address }}">
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="phone" class="control-label">Teléfono Fijo</label>
                                        <input type="text" placeholder="" data-mask="999 999" class="form-control" id="phone" name="phone" value="{{ $postulant->phone }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cellphone" class="control-label">Teléfono Móvil</label>
                                        <input type="text" placeholder="" data-mask="999 999 999" class="form-control" id="cellphone" name="cellphone" value="{{ $postulant->cellphone }}">
                                    </div>
                                </div>
                            </div>
                        </div><!-- end col -->
                        <div class="col-md-6">
                            <div class="form-group m-t-15">
                                <label for="career_id" class="control-label">Carrera</label>
                                <select id="career_id" class="form-control" name="career_id">
                                    <option value="">Seleccione carrera</option>
                                    @foreach($careers as $career)
                                        <option value="{{ $career->id }}" {{ $postulant->career_id == $career->id ? 'selected' : ''  }}>{{ $career->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="email" class="control-label">Email</label>
                                <input type="text" class="form-control" id="email" name="email" placeholder="Ingresar email" value="{{ $postulant->email }}">
                            </div>
                            <div class="form-group">
                                <label for="password" class="control-label">Contraseña <small>(Ingresar solo si desea modificar)</small></label>
                                <input type="password" class="form-control" id="password" name="password" value="">
                            </div>
                            <div class="form-group m-t-20">
                                <label for="image">Imagen</label>
                                <input id="image" type="file" class="dropify" name="image" data-height="150" data-default-file="{{ $postulant->image_url }}"/>
                            </div>
                        </div><!-- end row -->
                        <div class="col-sm-12">
                            <div class="form-group">
                                <button class="btn btn-success btn-bordred m-t-30">
                                    Guardar cambios
                                    <i class="fa fa-save"></i>
                                </button>
                                <a href="{{ url('admin/postulants') }}" class="btn btn-default btn-bordred m-t-30">
                                    Cancelar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div> <!-- container -->
    </div> <!-- content -->
@endsection
@section('scripts')
    <script src="{{ asset('plugins/fileuploads/js/dropify.min.js') }}"></script>
    <script>
        $('.dropify').dropify({
            messages: {
                'default': 'Drag and drop a file here or click',
                'replace': 'Drag and drop or click to replace',
                'remove': 'Remove',
                'error': 'Ooops, something wrong appended.'
            },
            error: {
                'fileSize': 'The file size is too big (1M max).'
            }
        });
    </script>
    <script src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        jQuery('#datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true
        });
    </script>
@endsection
