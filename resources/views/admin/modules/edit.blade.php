@extends('layouts.app')

@section('page-title')
    <a href="{{ url('/admin/modules') }}">Módulos</a> >
    Editar
@endsection
@section('styles')
@endsection
@section('content')
    <div class="content">
        <div class="container">
            @include('includes.alerts')
            <form role="form"  action="" method="POST">
                @csrf
                <div class="row">
                    <div class="col-sm-6">
                        <div class="card-box">
                            <h4 class="m-t-0 header-title m-t-0 m-b-20"><b>Datos del módulo</b></h4>
                            <div class="form-group">
                                <label for="name" class="control-label">Nombre</label>
                                <input type="text" placeholder="Nombre del módulo" class="form-control" id="name" name="name" value="{{ $module->name }}">
                            </div>
                            <div class="form-group m-t-15">
                                <label for="group_module_id" class="control-label">Grupo Módulo</label>
                                <select class="form-control" name="group_module_id" id="group_module_id">
                                    <option value="">Seleccione grupo módulo</option>
                                    @foreach($groups as $group)
                                        <option value="{{ $group->id }}" {{ $module->group_module_id == $group->id ? 'selected' : ''  }}>{{ $group->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group m-t-15">
                                <label for="career_id" class="control-label">Carrera</label>
                                <select class="form-control" name="career_id" id="career_id">
                                    <option value="">Seleccione carrera</option>
                                    @foreach($careers as $career)
                                        <option value="{{ $career->id }}" {{ $module->career_id == $career->id ? 'selected' : ''  }}>{{ $career->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="description">Descripción</label>
                                <textarea name="description" id="description" class="form-control">{{ $module->description }}</textarea>
                            </div>
                            <div class="form-group" >
                                <button class="btn btn-success btn-bordred m-t-20">
                                    Guardar cambios
                                    <i class="fa fa-save"></i>
                                </button>
                                <a href="{{ url('admin/modules') }}" class="btn btn-default btn-bordred m-t-20">
                                    Cancelar
                                </a>
                            </div>
                        </div>
                    </div><!-- end col -->
                    <!-- end row -->
            </form>
        </div> <!-- container -->
    </div> <!-- content -->
@endsection
@section('scripts')
@endsection
