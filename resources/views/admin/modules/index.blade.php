@extends('layouts.app')

@section('icon')
@endsection

@section('page-title')
Módulos
@endsection

@section('content')
    <div class="content">
        <div class="container">
            @if (session('notification'))
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('notification') }}
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-sm-8">
                    <a href="{{ url('admin/modules/create') }}" class="btn btn-success btn-md waves-effect waves-light m-b-30"
                       data-overlaySpeed="200" data-overlayColor="#36404a">Nuevo Módulo</a>
                </div>
            </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">Lista de módulos</h4>
                            <div class="table-responsive">
                                <table class="table m-0">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nombre</th>
                                        <th>Tipo</th>
                                        <th>Carrera</th>
                                        <th>Acciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($modules as $module)
                                        <tr>
                                            <th scope="row">{{ $module->id }}</th>
                                            <td>{{ $module->name }}</td>
                                            <td>{{ $module->group->name }}</td>
                                            <td>{{ $module->career->name }}</td>
                                            <td>
                                                <a href="{{ url('admin/modules/'.$module->id.'/edit') }}" class="btn btn-sm btn-primary" title="Editar">
                                                    <i class="fa fa-pencil-square-o"></i>
                                                </a>
                                                <button class="btn btn-sm btn-danger" title="Eliminar" data-delete="{{ url('admin/modules/'.$module->id.'/delete') }}">
                                                    <i class="fa fa-trash o"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!-- end col -->

                </div>
{{--                {{ $evaluators->render() }}--}}
        </div>
    </div>
@endsection

@section('scripts')
    <!-- Sweet alert 2 -->
    <script src="https://unpkg.com/sweetalert2@7.3.0/dist/sweetalert2.all.js"></script>
    <script>
        $(document).ready(function() {
            $('[data-delete]').on('click', onClickModuleDelete);
        });

        function onClickModuleDelete() {
            let urlDelete = $(this).data('delete');
            swal({
                title: '¿Seguro que desea eliminar este módulo?',
                text: "",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',

                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sí, eliminar!'
            }).then((result) => {
                if (result.value) {
                location.href = urlDelete;
            }
        });
        }
    </script>
@endsection