@extends('layouts.app')

@section('icon')
@endsection

@section('page-title')
    Asistencias
@endsection

@section('content')
    <div class="content">
        <div class="container">
            @if (session('notification'))
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('notification') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('error') }}
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-sm-8">
                    <a href="{{ url('admin/assistances/create') }}" class="btn btn-success btn-md waves-effect waves-light m-b-30"
                       data-overlaySpeed="200" data-overlayColor="#36404a">Registrar nueva asistencia</a>
                </div><!-- end col -->
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-box">
                        <h4 class="header-title m-t-0 m-b-30">Lista de asistencias</h4>
                        <div class="table-responsive">
                            <table class="table m-0">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Carrera</th>
                                    <th>Curso</th>
                                    <th>Fecha</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($assistances as $assistance)
                                    <tr>
                                        <th scope="row">{{ $assistance->id }}</th>
                                        <td>{{ $assistance->career->name }}</td>
                                        <td>{{ $assistance->course->name }}</td>
                                        <td>{{ $assistance->date_format }}</td>
                                        <td>
                                            <a href="{{ url('admin/assistances/'.$assistance->id.'/edit') }}" class="btn btn-sm btn-primary" title="Editar">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </a>
                                            <button class="btn btn-sm btn-danger" title="Eliminar" data-delete="{{ url('admin/assistances/'.$assistance->id.'/delete') }}">
                                                <i class="fa fa-trash o"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('[data-delete]').on('click', onClickAssistanceDelete);
        });
        function onClickAssistanceDelete() {
            let urlDelete = $(this).data('delete');
            swal({
                title: '¿Seguro que desea eliminar esta asistencia?',
                text: "",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',

                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sí, eliminar!'
            }).then((result) => {
                if (result.value) {
                location.href = urlDelete;
            }
        });
        }
    </script>
@endsection