@extends('layouts.app')

@section('icon')
@endsection
@section('styles')
    <link href="{{ asset('plugins/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection

@section('page-title')
Asistencia
@endsection

@section('content')
    <div class="content">
        <div class="container">
            @if (session('notification'))
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('notification') }}
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="" method="POST">
                @csrf
                <div class="card-box">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="select_career" class="control-label">Carrera</label>
                                <select class="form-control" name="career_id" id="select_career" data-user-id="{{$user->id}}" required>
                                    <option value="">Seleccione Carrera</option>
                                    @foreach($careers as $career)
                                        <option value="{{ $career->id }}" {{ old('career_id', $assistance->career_id) == $career->id ? 'selected' : '' }}>{{ $career->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div><!-- end col -->
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="select_course" class="control-label">Curso</label>
                                <select class="form-control" id="select_course" name="course_id" required>
                                    <option value="">Seleccione curso</option>
                                    @foreach($assistance->career->courses as $course)
                                        <option value="{{ $course->id }}" {{ old('course_id', $assistance->course_id) == $course->id ? 'selected' : '' }}>{{ $course->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div><!-- end col -->
                        <div class="col-sm-4 m-b-10">
                            <div class="form-group">
                                <label class="control-label">Fecha</label>
                                <input type="text" class="form-control" name="date" id="datepicker-autoclose" value="{{ $assistance->date_format }}" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">Lista de matrículas</h4>
                    <div class="table-responsive">
                        <table class="table m-0">
                            <thead>
                            <tr>
                                <th>Código</th>
                                <th>Alumno</th>
                                <th>Ciclo</th>
                                <th>Asistió</th>
                                <th>Faltó</th>
                            </tr>
                            </thead>
                            <tbody id="contentPostulants">
                                @foreach($assistance->details as $detail)
                                    <tr>
                                        <th id="code">{{ $detail->enrollment->postulant->code }}</th>
                                        <td id="name">{{ $detail->enrollment->postulant->name_complete }}</td>
                                        <td id="cycle">{{ $detail->enrollment->cycle }}</td>
                                        <td>
                                            <div class="radio radio-inline">
                                                <input type="hidden" value="{{ $detail->enrollment_id }}" name="enrollmentIds[]">
                                                <input type="radio" id="assist{{ $detail->enrollment_id }}" value="1"
                                                       name="assistance{{ $detail->enrollment_id }}" required {{ $detail->assistance ? 'checked' : '' }}>
                                                <label for="man">  </label>
                                                </div>
                                            </td>
                                        <td>
                                            <div class="radio radio-purple radio-inline">
                                                <input type="radio" id="lack{{ $detail->enrollment_id }}" value="0"
                                                       name="assistance{{ $detail->enrollment_id }}" {{ !$detail->assistance ? 'checked' : '' }}>
                                                <label for="woman">  </label>
                                                </div>
                                            </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div id="errorMessage"></div>
                    <div class="form-group" >
                        <button class="btn btn-primary btn-bordred" id="btnRegister">
                            Guardar cambios <i class="fa fa-save"></i>
                        </button>
                        <a href="{{ url('admin/assistances') }}" class="btn btn-default btn-bordred">Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        jQuery('#datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true
        });
    </script>
    <script>
        $(function() {
            $('#select_career').on('change', onSelectCourseChange);
            $('#datepicker-autoclose, #select_course, #select_career').on('change', onShowPostulants);
        });

        function onSelectCourseChange() {
            var careerId = $(this).val();
            const userId = $(this).data('user-id');
            if (!careerId) {
                $('#select_course').html('<option value="">Seleccione curso</option>');
                $('#select_course').prop('disabled', true);
                return;
            }
            // AJAX
            $.get('/api/career/'+careerId+'/'+userId+'/courses', function (data) {
                var html_select = '<option value="">Seleccione curso</option>';
                for (var i=0; i<data.length; ++i)
                    html_select += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
                $('#select_course').html(html_select);
                $('#select_course').prop('disabled', false);
            });
        }

        function onShowPostulants() {
            var careerId = $('#select_career').val();
            var courseId = $('#select_course').val();
            var $date = $('#datepicker-autoclose').val();
            const $contentPostulant = $('#contentPostulants');
            const $btnRegister = $('#btnRegister');
            const $message = $('#errorMessage');

            if (careerId && courseId && $date) {
                $contentPostulant.append('<i class="fa fa-2x fa-spinner fa-spin"></i>');
                // AJAX
                $.get('/api/postulants/'+careerId+'/'+courseId+'/?date='+$date, function (enrollments) {

                    $contentPostulant.empty();

                    if(enrollments.length > 0) {
                        for (var i=0; i<enrollments.length; ++i) {
                            var enrollment = enrollments[i];
                            $contentPostulant.append('<tr>\n' +
                                '<th id="code">'+enrollment.postulant.code+'</th>\n' +
                                '<td id="name">'+enrollment.postulant.name_complete+'</td>\n' +
                                '<td id="cycle">'+enrollment.cycle+'</td>\n' +
                                '<td>\n' +
                                '<div class="radio radio-inline">\n' +
                                '<input type="hidden" value="'+enrollment.id+'" name="enrollmentIds[]">' +
                                '<input type="radio" id="assist'+enrollment.id+'" value="1" name="assistance'+enrollment.id+'" required>\n' +
                                '<label for="man">  </label>\n' +
                                '</div>\n' +
                                '</td>\n' +
                                '<td>\n' +
                                '<div class="radio radio-purple radio-inline">\n' +
                                '<input type="radio" id="lack'+enrollment.id+'" value="0" name="assistance'+enrollment.id+'" >\n' +
                                '<label for="woman">  </label>\n' +
                                '</div>\n' +
                                '</td>\n' +
                                '</tr>');
                        }
                        $message.empty();
                        $btnRegister.show();
                    } else {
                        $message.html('No se encontraron alumnos inscritos a este curso');
                        $btnRegister.hide();
                    }
                });
            }
        }
    </script>
@endsection