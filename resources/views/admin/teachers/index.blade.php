@extends('layouts.app')

@section('icon')
@endsection

@section('page-title')
Docentes
@endsection

@section('content')
    <div class="content">
        <div class="container">
            @include('includes.alerts')
            <div class="row">
                <div class="col-sm-8">
                    <a href="{{ url('admin/teachers/create') }}" class="btn btn-success btn-md waves-effect waves-light m-b-30"
                       data-overlaySpeed="200" data-overlayColor="#36404a"><i class="zmdi zmdi-account-add"></i> Nuevo docente</a>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-box">
                        <h4 class="header-title m-t-0 m-b-30">Lista de docentes</h4>
                        <div class="table-responsive">
                            <table class="table m-0">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>DNI</th>
                                    <th>Apellidos</th>
                                    <th>Nombres</th>
                                    <th>Teléfono</th>
                                    <th>Género</th>
                                    <th>Carrera</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($teachers as $teacher)
                                    <tr>
                                        <th scope="row">{{ $teacher->id }}</th>
                                        <td>{{ $teacher->dni }}</td>
                                        <td>{{ $teacher->last_name }}</td>
                                        <td>{{ $teacher->first_name }}</td>
                                        <td>{{ $teacher->phone }}</td>
                                        <td>{{ $teacher->gender_name }}</td>
                                        <td>{{ $teacher->career ? $teacher->career->name : '' }}</td>
                                        <td>
                                            <a href="{{ url('admin/teachers/'.$teacher->id.'/edit') }}" class="btn btn-sm btn-primary" title="Editar">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </a>
                                            <button class="btn btn-sm btn-danger" title="Eliminar" data-delete="{{ url('admin/teachers/'.$teacher->id.'/delete') }}">
                                                <i class="fa fa-trash o"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!-- end col -->

            </div>
{{--                {{ $evaluators->render() }}--}}
        </div>
    </div>
@endsection

@section('scripts')
    <!-- Sweet alert 2 -->
    <script src="https://unpkg.com/sweetalert2@7.3.0/dist/sweetalert2.all.js"></script>
    <script>
        $(document).ready(function() {
            $('[data-delete]').on('click', onClickTeacherDelete);
        });

        function onClickTeacherDelete() {
            let urlDelete = $(this).data('delete');
            swal({
                title: '¿Seguro que desea eliminar este docente?',
                text: "",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',

                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sí, eliminar!'
            }).then((result) => {
                if (result.value) {
                    location.href = urlDelete;
                }
            });
        }
    </script>
@endsection