@extends('layouts.app')

@section('page-title')
    <a href="{{ url('admin/teachers') }}">Docentes</a> > Nuevo
@endsection

@section('styles')
    <!-- form Uploads -->
    <link href="{{ asset('plugins/fileuploads/css/dropify.min.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <div class="content">
        <div class="container">
            @include('includes.alerts')
            <form role="form"  action="" method="POST">
                @csrf
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="m-t-0 header-title m-t-0 m-b-20"><b>Datos del docente</b></h4>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="dni" class="control-label">DNI</label>
                                        <input type="text" placeholder="" data-mask="99999999" class="form-control" id="dni" name="dni" value="{{ old('dni') }}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <p class="control-label font-600">Género</p>
                                    <div class="radio radio-inline">
                                        <input type="radio" id="man" value="M" name="gender" {{ old('gender') == 'M' ? 'checked' : '' }}>
                                        <label for="man"> Masculino </label>
                                    </div>
                                    <div class="radio radio-purple radio-inline">
                                        <input type="radio" id="woman" value="F" name="gender" {{ old('gender') == 'F' ? 'checked' : '' }}>
                                        <label for="woman"> Femenino </label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="first_name" class="control-label">Nombre(s)</label>
                                        <input type="text" class="form-control" id="first_name" placeholder="Ingresar nombre" name="first_name" value="{{ old('first_name') }}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="last_name" class="control-label">Apellidos</label>
                                        <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Ingresar apellidos" value="{{ old('last_name') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="address" class="control-label">Dirección</label>
                                <input type="text" class="form-control" id="address" name="address" placeholder="Ingresar dirección" value="{{ old('address') }}">
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="phone" class="control-label">Teléfono Fijo</label>
                                        <input type="text" placeholder="" data-mask="999 999" class="form-control" id="phone" name="phone" value="{{ old('phone') }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cellphone" class="control-label">Teléfono Móvil</label>
                                        <input type="text" placeholder="" data-mask="999 999 999" class="form-control" id="cellphone" name="cellphone" value="{{ old('cellphone') }}">
                                    </div>
                                </div>
                            </div>
                        </div><!-- end col -->
                        <div class="col-md-6">
                            <div class="form-group m-t-15">
                                <label for="career_id" class="control-label">Carrera</label>
                                <select class="form-control" name="career_id">
                                    <option value="">Seleccione carrera</option>
                                    @foreach($careers as $career)
                                        <option value="{{ $career->id }}" {{ old('career_id') == $career->id ? 'selected' : ''  }}>{{ $career->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="email" class="control-label">Email</label>
                                <input type="text" class="form-control" id="email" name="email" placeholder="Ingresar email" value="{{ old('email') }}">
                            </div>
                            <div class="form-group">
                                <label for="password" class="control-label">Contraseña</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="" value="{{ old('password') }}">
                            </div>
                            <div class="form-group m-t-20">
                                <label for="image">Imagen</label>
                                <input id="image" type="file" name="image" class="dropify" data-height="150"/>
                            </div>
                        </div><!-- end row -->
                        <div class="col-sm-12">
                            <div class="form-group">
                                <button class="btn btn-primary btn-bordred m-t-30">
                                    Registrar
                                    <i class="fa fa-save"></i>
                                </button>
                                <a href="{{ url('admin/teachers') }}" class="btn btn-default btn-bordred m-t-30">
                                    Cancelar
                                </a>
                            </div>
                        </div>
                    </div><!-- end col -->
                </div>
            </form>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('plugins/fileuploads/js/dropify.min.js') }}"></script>
    <script>
        $('.dropify').dropify({
            messages: {
                'default': 'Drag and drop a file here or click',
                'replace': 'Drag and drop or click to replace',
                'remove': 'Remove',
                'error': 'Ooops, something wrong appended.'
            },
            error: {
                'fileSize': 'The file size is too big (1M max).'
            }
        });
    </script>
@endsection
