@extends('layouts.app')

@section('icon')
@endsection

@section('page-title')
Cursos
@endsection

@section('content')
    <div class="content">
        <div class="container">
            @if (session('notification'))
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('notification') }}
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-sm-8">
                    <a href="{{ url('admin/courses/create') }}" class="btn btn-success btn-md waves-effect waves-light m-b-30"
                       data-overlaySpeed="200" data-overlayColor="#36404a"><i class="zmdi zmdi-account-add"></i> Nuevo curso</a>
                </div><!-- end col -->
                {{--<form action="">--}}
                    {{--<div class="col-sm-4">--}}
                        {{--<div class="input-group">--}}
                            {{--<input type="text" name="search_evaluator" class="form-control" placeholder="Buscar evaluador..." value="{{ $searchEvaluator }}">--}}
                            {{--<span class="input-group-btn">--}}
                                {{--<button type="submit" class="btn waves-effect waves-light btn-purple">--}}
                                    {{--<i class="fa fa-search"></i>--}}
                                {{--</button>--}}
                            {{--</span>--}}
                        {{--</div>--}}
                    {{--</div><!-- end col -->--}}
                {{--</form>--}}
            </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">Lista de cursos</h4>
                            <div class="table-responsive">
                                <table class="table m-0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Carrera</th>
                                            <th>Nombre</th>
                                            <th>Módulo</th>
                                            {{--<th>Descripción</th>--}}
                                            <th>Ciclo</th>
                                            <th>Créditos</th>
                                            <th>Horas totales</th>
                                            <th>Criterio evaluación</th>
                                            <th class="col-sm-2">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($courses as $course)
                                            <tr>
                                                <th scope="row">{{ $course->id }}</th>
                                                <td>{{ $course->career->name }}</td>
                                                <td>{{ $course->name }}</td>
                                                <td>{{ $course->module->name }}</td>
                                                {{--<td>Table cell</td>--}}
                                                <td>{{ $course->cycle }}</td>
                                                <td>{{ $course->credits }}</td>
                                                <td>{{ $course->total_hours }}</td>
                                                <td>{{ $course->evaluation_criteria }}</td>
                                                <td>
                                                    <a href="{{ url('admin/courses/'.$course->id.'/edit') }}" class="btn btn-sm btn-primary" title="Editar">
                                                        <i class="fa fa-pencil-square-o"></i>
                                                    </a>
                                                    <button class="btn btn-sm btn-danger" title="Eliminar" data-delete="{{ url('admin/courses/'.$course->id.'/delete') }}">
                                                        <i class="fa fa-trash o"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!-- end col -->

                </div>
{{--                {{ $evaluators->render() }}--}}
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('[data-delete]').on('click', onClickCourseDelete);
        });

        function onClickCourseDelete() {
            let urlDelete = $(this).data('delete');
            swal({
                title: '¿Seguro que desea eliminar este curso?',
                text: "",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',

                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sí, desactivar!'
            }).then((result) => {
                if (result.value) {
                    location.href = urlDelete;
                }
            });
        }
    </script>
@endsection