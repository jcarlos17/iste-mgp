@extends('layouts.app')

@section('page-title')
    <a href="{{ url('/admin/courses') }}">Cursos</a> >
    Nuevo
@endsection
@section('styles')
@endsection
@section('content')
    <div class="content">
        <div class="container">
            @if (session('notification'))
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('notification') }}
                </div>
            @endif

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form role="form"  action="" method="POST">
                @csrf
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="m-t-0 header-title m-t-0 m-b-20"><b>Datos del curso</b></h4>
                            <div class="form-group m-t-15">
                                <label for="career_id" class="control-label">Carrera</label>
                                <select class="form-control" name="career_id" id="career_id">
                                    <option>Seleccione carrera</option>
                                    @foreach($careers as $career)
                                        <option value="{{ $career->id }}" {{ old('career_id') == $career->id ? 'selected' : '' }}>{{ $career->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="select_group_module" class="control-label">Módulo</label>
                                <select class="form-control" id="select_group_module" name="group_module_id" disabled>
                                    <option value="">Seleccione tipo de módulo</option>
                                    @foreach($groupModules as $groupModule)
                                        <option value="{{ $groupModule->id }}">{{ $groupModule->name }}</option>
                                    @endforeach
                                </select>
                                <select class="form-control m-t-10" id="select_module" name="module_id" disabled>
                                    <option>Seleccione módulo</option>
                                </select>
                            </div>
                        </div><!-- end col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="control-label">Nombre</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cycle" class="control-label">Ciclo</label>
                                        <select class="form-control" id="cycle" name="cycle">
                                            <option>Seleccione ciclo</option>
                                            <option value="I" {{ old('cycle') == 'I' ? 'selected' : '' }}>I</option>
                                            <option value="II" {{ old('cycle') == 'II' ? 'selected' : '' }}>II</option>
                                            <option value="III" {{ old('cycle') == 'III' ? 'selected' : '' }}>III</option>
                                            <option value="IV" {{ old('cycle') == 'IV' ? 'selected' : '' }}>IV</option>
                                            <option value="V" {{ old('cycle') == 'V' ? 'selected' : '' }}>V</option>
                                            <option value="VI" {{ old('cycle') == 'VI' ? 'selected' : '' }}>VI</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="credits" class="control-label">Créditos</label>
                                        <input type="text" class="form-control" id="credits" name="credits" value="{{ old('credits') }}" onkeypress="return justNumbers(event);">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="total_hours" class="control-label">Horas totales</label>
                                        <input type="text" class="form-control" id="total_hours" name="total_hours" value="{{ old('total_hours') }}" onkeypress="return justNumbers(event);">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="evaluation_criteria" class="control-label">Criterios de evaluación</label>
                                        <input type="text" class="form-control" id="evaluation_criteria" name="evaluation_criteria" value="{{ old('evaluation_criteria') }}" onkeypress="return justNumbers(event);">
                                    </div>
                                </div>
                            </div>
                        </div><!-- end row -->
                    </div><!-- end row -->
                    <div class="form-group">
                        <button class="btn btn-primary btn-bordred m-l-15 m-t-30">
                            Registrar
                            <i class="fa fa-save"></i>
                        </button>
                        <a href="{{ url('admin/courses') }}" class="btn btn-default btn-bordred m-l-15 m-t-30">
                            Cancelar
                        </a>
                    </div>
                </div>
            </form>
        </div> <!-- container -->
    </div> <!-- content -->
@endsection
@section('scripts')
    <script>
        function justNumbers(e)
        {
            var keynum = window.event ? window.event.keyCode : e.which;
            if (keynum === 8 || keynum === 46)
                return true;
            return /\d/.test(String.fromCharCode(keynum));
        }
    </script>
    <script>
        $(function() {
            $('#career_id').on('change', onSelectGroupModuleChange);
            $('#select_group_module').on('change', onSelectModuleChange);
        });

        function onSelectGroupModuleChange() {
            var careerId = $(this).val();
            if (!careerId) {
                $('#select_group_module').prop('disabled', true);
            }else{
                $('#select_group_module').prop('disabled', false);
            }
        }

        function onSelectModuleChange() {
            var careerId = $('#career_id').val();
            var groupModuleId = $(this).val();
            if (!careerId || !groupModuleId) {
                $('#select_module').html('<option value="">Seleccione módulo</option>');
                $('#select_module').prop('disabled', true);
                return;
            }
            // AJAX
            $.get('/api/career/'+careerId+'/module/'+groupModuleId, function (data) {
                var html_select = '<option value="">Seleccione módulo</option>';
                for (var i=0; i<data.length; ++i)
                    html_select += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
                $('#select_module').html(html_select);
                $('#select_module').prop('disabled', false);
            });
        }
    </script>
@endsection
