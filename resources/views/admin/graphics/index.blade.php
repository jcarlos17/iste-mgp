@extends('layouts.app')

@section('page-title')
    Gráficos
@endsection

@section('content')
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="card-box">
                        <div id="container"></div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card-box">
                        <form class="form-inline" role="form" action="" method="GET">
                            <div class="form-group m-l-10">
                                <select name="semester_id" id="" class="form-control" onchange="$('#btnContinue').click()">
                                    <option value="">Seleccionar semestre</option>
                                    @foreach($semestersAll as $semester)
                                        <option value="{{ $semester->id }}" {{ $semester->id == $semesterId ? 'selected' : '' }}>{{ $semester->year_period }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <button type="submit" id="btnContinue" class="btn btn-success" style="display: none"></button>
                        </form>
                        <div id="container2"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-more.js"></script>
    <script>
        Highcharts.chart('container', {
            credits: {
                enabled: false
            },
            title: {
                text: 'Cantidad total de matriculados en los últimos 6 semestres'
            },

            subtitle: {
                text: 'Plain'
            },

            xAxis: {
                categories: [
                    @foreach($semesters as $semester)
                    '{{$semester->year_period}}',
                    @endforeach
                ]
            },

            series: [{
                type: 'column',
                name: 'Matriculados',
                colorByPoint: true,
                data: [
                    @foreach($semesters as $semester)
                        {{$semester->enrollments->count()}},
                    @endforeach
                ],
                showInLegend: false
            }]

        });
    </script>
    <script>
        Highcharts.chart('container2', {
            credits: {
                enabled: false
            },
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            title: {
                text: 'Matriculados en las carreras'
            },
            tooltip: {
                pointFormat: '<b>{series.name}:</b> {point.y} ({point.percentage:.1f}%)'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    depth: 35,
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}'
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Matriculados',
                data: [
                    @foreach($careers as $career)
                        ['{{ $career->name }}', {{ $career->enrollments_count }}],
                    @endforeach
                ]
            }]
        });
    </script>
@endsection
