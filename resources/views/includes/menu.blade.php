<div id="sidebar-menu">
    <ul>
        <li class="text-muted menu-title">Menú</li>
        <li>
            <a href="/home" class="waves-effect"><i class="zmdi zmdi-home"></i> <span>Inicio</span></a>
        </li>
        @if(auth()->user()->is_admin || auth()->user()->is_secretary)
        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-collection-text"></i> <span> Datos generales </span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
                <li><a href="{{ asset('admin/teachers') }}">Docente</a></li>
                <li><a href="{{ asset('admin/postulants') }}">Postulante</a></li>
                <li><a href="{{ asset('admin/semesters') }}">Semestre</a></li>
                <li><a href="{{ asset('admin/careers') }}">Carrera</a></li>
                <li><a href="{{ asset('admin/modules') }}">Módulo</a></li>
                <li><a href="{{ asset('admin/courses') }}">Curso</a></li>
            </ul>
        </li>
        @endif
        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-edit"></i> <span> Registros </span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
                @if(auth()->user()->is_secretary || auth()->user()->is_admin)
                    <li><a href="{{ url('admin/workload') }}">Carga Lectiva</a></li>
                    <li><a href="{{ url('admin/enrollments') }}">Matrícula</a>
                @endif
                @if(auth()->user()->is_teacher)
                    <li><a href="{{ url('admin/assistances') }}">Asistencias</a></li>
                    <li><a href="{{ url('admin/notes') }}">Notas</a></li>
                @endif
            </ul>
        </li>

        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-chart"></i> <span> Reportes </span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
                {{--<li><a href="#">Postulantes</a></li>--}}
                @if(auth()->user()->is_admin || auth()->user()->is_secreatry)
                <li><a href="{{ url('admin/reports/enrollments') }}">Matriculados</a></li>
                @endif
                {{--<li><a href="#">Docentes</a></li>--}}
                @if(auth()->user()->is_teacher || auth()->user()->is_admin || auth()->user()->is_secreatry)
                    <li><a href="{{ url('admin/reports/assistances') }}">Asistencia</a></li>
                    <li><a href="{{ url('admin/reports/notes') }}">Notas</a></li>
                @endif
            </ul>
        </li>
        @if(auth()->user()->is_admin)
        <li>
            <a href="{{ url('admin/graphics') }}" class="waves-effect"><i class="fa fa-pie-chart"></i> <span>Gráficos</span></a>
        </li>
        @endif
    </ul>
    <div class="clearfix"></div>
</div>