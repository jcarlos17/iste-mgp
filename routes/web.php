<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// namespace -> Admin
Route::group(['middleware' => ['auth','secretary'], 'namespace' => 'Admin', 'prefix' => 'admin'], function (){
    // Admin->teachers
    Route::get('teachers', 'TeacherController@index');
    Route::get('teachers/create', 'TeacherController@create');
    Route::post('teachers/create', 'TeacherController@store');
    Route::get('teachers/{id}/edit', 'TeacherController@edit');
    Route::post('teachers/{id}/edit', 'TeacherController@update');
    Route::get('teachers/{id}/delete', 'TeacherController@delete');
    // Admin->postulants
    Route::get('postulants', 'PostulantController@index');
    Route::get('postulants/create', 'PostulantController@create');
    Route::post('postulants/create', 'PostulantController@store');
    Route::get('postulants/{id}/edit', 'PostulantController@edit');
    Route::post('postulants/{id}/edit', 'PostulantController@update');
    Route::get('postulants/{id}/delete', 'PostulantController@delete');
    // Admin->semesters
    Route::get('semesters', 'SemesterController@index');
    Route::get('semesters/create', 'SemesterController@create');
    Route::post('semesters/create', 'SemesterController@store');
    Route::get('semesters/{id}/edit', 'SemesterController@edit');
    Route::post('semesters/{id}/edit', 'SemesterController@update');
    Route::get('semesters/{id}/delete', 'SemesterController@delete');
    // Admin->careers
    Route::get('careers', 'CareerController@index');
    Route::get('careers/create', 'CareerController@create');
    Route::post('careers/create', 'CareerController@store');
    Route::get('careers/{id}/edit', 'CareerController@edit');
    Route::post('careers/{id}/edit', 'CareerController@update');
    Route::get('careers/{id}/delete', 'CareerController@delete');
    Route::get('careers/{id}/modules', 'CareerController@modules');

    // Admin->modules
    Route::get('modules', 'ModuleController@index');
    Route::get('modules/create', 'ModuleController@create');
    Route::post('modules/create', 'ModuleController@store');
    Route::get('modules/{id}/edit', 'ModuleController@edit');
    Route::post('modules/{id}/edit', 'ModuleController@update');
    Route::get('modules/{id}/delete', 'ModuleController@delete');

    // Admin->courses
    Route::get('courses', 'CourseController@index');
    Route::get('courses/create', 'CourseController@create');
    Route::post('courses/create', 'CourseController@store');
    Route::get('courses/{id}/edit', 'CourseController@edit');
    Route::post('courses/{id}/edit', 'CourseController@update');
    Route::get('courses/{id}/delete', 'CourseController@delete');
    // Admin->workload
    Route::get('workload', 'WorkloadController@index');
    Route::get('workload/create', 'WorkloadController@create');
    Route::post('workload/create', 'WorkloadController@store');
    Route::get('workload/{id}/edit', 'WorkloadController@edit');
    Route::post('workload/{id}/edit', 'WorkloadController@update');
    Route::get('workload/{id}/delete', 'WorkloadController@delete');
    // Admin->enrollment
    Route::get('enrollments', 'EnrollmentController@index');
    Route::get('enrollments/create', 'EnrollmentController@create');
    Route::post('enrollments/create', 'EnrollmentController@store');
    Route::get('enrollments/{id}/edit', 'EnrollmentController@edit');
    Route::post('enrollments/{id}/edit', 'EnrollmentController@update');
    Route::get('enrollments/{id}/delete', 'EnrollmentController@delete');

    Route::group(['namespace' => 'Reports','prefix' => 'reports'], function (){
        Route::get('enrollments', 'EnrollmentController@index');
    });

    Route::get('graphics', 'GraphicController@index');
});

Route::group(['middleware' => ['auth','teacher'], 'namespace' => 'Admin', 'prefix' => 'admin'], function () {
    // teacher->assistance
    Route::get('/assistances', 'AssistanceController@index');
    Route::get('/assistances/create', 'AssistanceController@create');
    Route::post('/assistances/create', 'AssistanceController@store');
    Route::get('/assistances/{id}/edit', 'AssistanceController@edit');
    Route::post('/assistances/{id}/edit', 'AssistanceController@update');
    Route::get('/assistances/{id}/delete', 'AssistanceController@delete');
    // teacher->notes
    Route::get('/notes', 'NoteController@index');
    Route::get('/notes/create', 'NoteController@create');
    Route::post('/notes/create', 'NoteController@store');
    Route::get('/notes/{id}/edit', 'NoteController@edit');
    Route::post('/notes/{id}/edit', 'NoteController@update');
    Route::get('/notes/{id}/delete', 'NoteController@delete');
    Route::group(['namespace' => 'Reports','prefix' => 'reports'], function (){
        Route::get('assistances', 'AssistanceController@index');
        Route::get('notes', 'NoteController@index');
    });
});
