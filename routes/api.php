<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// Web API
Route::get('/career/{careerId}/module/{moduleId}', 'Api\ModuleController@index');
Route::get('/career/{id}/courses', 'Api\CourseController@index');
Route::get('/course/{id}', 'Api\CourseController@show');
Route::get('/career/{id}/{userId}/courses', 'Api\CourseController@workload');
Route::get('/postulants', 'Api\PostulantController@show');
Route::get('/postulants/{careerId}/{courseId}', 'Api\PostulantController@enrollment');
